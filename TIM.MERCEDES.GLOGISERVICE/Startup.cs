using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Features;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.FileProviders;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using TIM.MERCEDES.BUSINESS.Abstract;
using TIM.MERCEDES.BUSINESS.Concrete;
using TIM.MERCEDES.DATAACCESS.Abstract;
using TIM.MERCEDES.DATAACCESS.Concrete.EntityFramework;
using TIM.MERCEDES.ENTITIES.Concrete.Settings;
using TIM.MERCEDES.LOGGER.Abstract;
using TIM.MERCEDES.LOGGER.Concrete.Log4Net;

namespace TIM.MERCEDES.GLOGISERVICE
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers().AddNewtonsoftJson(
            options =>
            {
                options.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore;
                options.SerializerSettings.NullValueHandling = Newtonsoft.Json.NullValueHandling.Ignore;
                options.SerializerSettings.TypeNameHandling = Newtonsoft.Json.TypeNameHandling.None;
            });

            services.AddHttpContextAccessor();

            services.Configure<MailSettings>(Configuration.GetSection("MailSettings"));


            services.AddScoped<IBBDataDAL, BBDataDAL>();
            services.AddTransient<ISeatDAL, SeatDAL>();
            services.AddTransient<IFaultyBBDAL, FaultyBBDAL>();
            services.AddTransient<IPairingHistoryDAL, PairingHistoryDAL>();
            services.AddTransient<ILoggerManager, Log4NetManager>();
            services.AddTransient<IMailService, MailService>();
            services.AddControllers();

            services.Configure<FormOptions>(x =>
            {
                x.ValueLengthLimit = int.MaxValue;
                x.MultipartBodyLengthLimit = int.MaxValue;
                x.MultipartHeadersLengthLimit = int.MaxValue;
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseRouting();

            app.UseAuthorization();

            app.UseStaticFiles(new StaticFileOptions
            {
                OnPrepareResponse = ctx => {
                    ctx.Context.Response.Headers.Append("Access-Control-Allow-Origin", "*");
                    ctx.Context.Response.Headers.Append("Access-Control-Allow-Headers",
                      "Origin, X-Requested-With, Content-Type, Accept");
                },
                FileProvider = new PhysicalFileProvider(Path.Combine(env.ContentRootPath, "faultyimages")),
                RequestPath = "/faultyimages"
            });

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
