﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TIM.MERCEDES.ENTITIES.Concrete;

namespace TIM.MERCEDES.GLOGISERVICE.Models
{
    public class OperationDataModel
    {
        public string Username { get; set; }
        public string Password { get; set; }
        //public Product Product { get; set; }
        public string FaultyBBId { get; set; }
        public string FaultySeatId { get; set; }
        public int FaultyBBStatus { get; set; }
        public bool FaultyBBReProductionStatus { get; set; }

        public string SelectedBBId { get; set; }

        public BBData BBNumberDetails { get; set; } = null;
        public PairingHistory PairingHistory { get; set; }

        public int MaterialsPageNumber { get; set; }
        public int CushionsPageNumber { get; set; }

        public string ImgName { get; set; } 

        public bool IsAuthenticated { get; set; } = false;
    }
}
