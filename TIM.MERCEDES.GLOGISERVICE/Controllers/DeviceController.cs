﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIM.MERCEDES.BUSINESS.Abstract;
using TIM.MERCEDES.BUSINESS.Concrete;
using TIM.MERCEDES.COMMON.Enums;
using TIM.MERCEDES.COMMON.Helpers;
using TIM.MERCEDES.DATAACCESS.Abstract;
using TIM.MERCEDES.ENTITIES.Concrete;
using TIM.MERCEDES.ENTITIES.Concrete.Models;
using TIM.MERCEDES.ENTITIES.Concrete.Models.RuntimeModels;
using TIM.MERCEDES.GLOGISERVICE.Helpers;
using TIM.MERCEDES.GLOGISERVICE.Models;
using TIM.MERCEDES.LOGGER.Abstract;
using TIM.SDK.GLOGI;
using TIM.SDK.GLOGI.Executions;
using TIM.SDK.GLOGI.Helpers;
using TIM.SDK.GLOGI.Models;

namespace TIM.MERCEDES.GLOGISERVICE.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DeviceController : ControllerBase
    {
        public Indicator _indicator { get; set; }

        private string _generalServiceUrl { get; set; }

        private string _cacheUrl { get; set; }

        private readonly IConfiguration Configuration;

        private BBDataManager bBDataervice;

        private FaultyBBManager faultyBBService;

        private PairingHistoryManager pairingHistoryService;

        private ILoggerManager loggerManager;

        private readonly IMailService _mailService;

        private readonly IWebHostEnvironment _env;

        public DeviceController(IConfiguration configuration, IBBDataDAL bBNumberDAL, IFaultyBBDAL faultyBBDAL, IPairingHistoryDAL pairingHistoryDAL, ILoggerManager _loggerManager, IWebHostEnvironment env, IMailService mailService)
        {
            bBDataervice = new BBDataManager(bBNumberDAL);
            faultyBBService = new FaultyBBManager(faultyBBDAL);
            pairingHistoryService = new PairingHistoryManager(pairingHistoryDAL);
            Configuration = configuration;
            _generalServiceUrl = Configuration["GeneralServiceUrl"];
            _cacheUrl = Configuration["CacheUrl"];
            loggerManager = _loggerManager;


            if (_indicator == null)
                _indicator = CommonHelper.DoInitIndicator();
            _env = env;
            _mailService = mailService;
        }


        [HttpGet]
        [Route("testme")]
        public string testme()
        {
            //var fullPath = Path.Combine(_env.ContentRootPath, "FaultyImages");


            //if (!Directory.Exists(fullPath)) Directory.CreateDirectory(fullPath);

            //var img = Image.FromFile("test.jpg");

            //var imgName = $"{Guid.NewGuid().ToString()}.jpg";

            //img.Save($"{fullPath}\\{imgName}", ImageFormat.Jpeg);

            //var test = bBDataervice.Get(item => item.Id == Convert.ToInt32(1), true, "BBDetails");
            //Request.Headers.Add("UserName", "TEST");

            //bBDataervice.Add(new BBData());
            return "i am ok";
        }

        [HttpGet]
        [Route("DeviceCall")]
        public Display DeviceCall(string id)
        {
            var cacheKey = CommonHelper.GenerateCacheKey(id);
            OperationDataModel operationalData = StateFactory<OperationDataModel>.GetOpetationData(_cacheUrl, cacheKey);

            if (operationalData == null)
            {
                operationalData = new OperationDataModel();
                StateFactory<OperationDataModel>.SetOpetationData(_cacheUrl, cacheKey, operationalData, DateTime.Now.AddMinutes(10));
            }

            var screenDesignHelper = new ScreenDesignHelper(Configuration, _indicator, _cacheUrl);

            var screen = new Screen();
            if (!operationalData.IsAuthenticated)
                screen = screenDesignHelper.GenerateLoginScreen();
            else
                screen = screenDesignHelper.GenerateMenuScreen();

            loggerManager.LogInformation(new StateLog(userName: "", methodName: nameof(DeviceCall), deviceId: id, message: "Initialized Device"));

            StateFactory<OperationDataModel>.InitializeStateMachine(_cacheUrl, cacheKey, DateTime.Now.AddMinutes(10), id, "GET");

            var display = screenDesignHelper.GenerateDisplayByGivenScreen(screen, operationalData);
            StateFactory<OperationDataModel>.AddOrUpdateStateMachineState(_cacheUrl, cacheKey, new StateModel { DoNotCall = false, Request = null, Response = display, ScreenId = "GET" }, true, DateTime.Now.AddMinutes(10));

            return display;
        }

        [HttpPost]
        [Route("DeviceCall")]
        public Display DeviceCall([FromBody] DeviceRequest model)
        {
            var cacheKey = CommonHelper.GenerateCacheKey(model.device.id);
            OperationDataModel operationalData = StateFactory<OperationDataModel>.GetOpetationData(_cacheUrl, cacheKey);

            try
            {
                if (operationalData == null)
                {
                    operationalData = new OperationDataModel();
                    StateFactory<OperationDataModel>.SetOpetationData(_cacheUrl, cacheKey, operationalData, DateTime.Now.AddMinutes(10));
                }

                var screenDesignHelper = new ScreenDesignHelper(Configuration, _indicator, _cacheUrl);

                StateFactory<OperationDataModel>.InitializeStateMachine(_cacheUrl, cacheKey, DateTime.Now.AddMinutes(10), model.device.id, "POST");

                var screen = new Screen();
                if (!operationalData.IsAuthenticated || model.data.@event.Equals("exit"))
                {
                    operationalData.IsAuthenticated = false;
                    screen = screenDesignHelper.GenerateLoginScreen();
                    StateFactory<OperationDataModel>.SetOpetationData(_cacheUrl, cacheKey, operationalData, DateTime.Now.AddMinutes(10));
                }
                else
                    screen = screenDesignHelper.GenerateMenuScreen();

                var display = screenDesignHelper.GenerateDisplayByGivenScreen(screen, operationalData);

                loggerManager.LogInformation(new StateLog(userName: operationalData.Username, methodName: nameof(DeviceCall), deviceId: model.device.id, message: "Re-Initialized Device"));

                StateFactory<OperationDataModel>.AddOrUpdateStateMachineState(_cacheUrl, cacheKey, new StateModel { DoNotCall = false, Request = null, Response = display, ScreenId = "MenuScreen" }, true, DateTime.Now.AddMinutes(10));

                return display;
            }
            catch (ScreenException ex)
            {
                loggerManager.LogInformation(new StateLog(userName: operationalData.Username, inputParameters: JsonConvert.SerializeObject(model), methodName: nameof(DeviceCall), deviceId: model.device.id, message: ex.ToString()));
                return ScreenDesignHelper.CreateException(ex.State, ex.Header, ex.ErrorMessage, ButtonTypes.exception, $"/{Constants.ServicePath}/device/DeviceCall");
            }
        }

        [HttpPost]
        [Route("MenuScreen")]
        public Display MenuScreen([FromBody] DeviceRequest model)
        {
            var cacheKey = CommonHelper.GenerateCacheKey(model.device.id);
            OperationDataModel operationalData = StateFactory<OperationDataModel>.GetOpetationData(_cacheUrl, cacheKey);
            try
            {
                this.CheckOperationalData(operationalData, model);
                this.SetUserInfoToRequestHeader(operationalData);

                loggerManager.LogInformation(new StateLog(userName: operationalData.Username, inputParameters: JsonConvert.SerializeObject(model), methodName: nameof(MenuScreen), deviceId: model.device.id));

                if (model.state.Equals("loginscreen"))
                {
                    operationalData.Username = model.data.input[0].value;
                    operationalData.Password = model.data.input[1].value;

                    loggerManager.LogInformation($"User: {operationalData.Username} , Pass: {operationalData.Password}");
                    var ldapDomain = Configuration["LDAP_DOMAIN"];
                    var ldapPort = Convert.ToInt32(Configuration["LDAP_PORT"]);
                    loggerManager.LogInformation($"LDAP_DOMAIN: {ldapDomain} , LDAP_PORT: {ldapPort}");
                    AuthenticationHelper authenticationHelper = new AuthenticationHelper(ldapDomain, ldapPort, loggerManager);

                    var response = authenticationHelper.AuthenticateWithLDAP(operationalData.Username, operationalData.Password);
                    loggerManager.LogInformation($"reponse: {response}");

                    //---------
                    if (!response)
                        return ScreenDesignHelper.CreateException(model.state, "HATA", "Hatalı kullanıcı bilgileri.", ButtonTypes.exception, $"/{Constants.ServicePath}/device/DeviceCall");

                    operationalData.IsAuthenticated = true;
                    StateFactory<OperationDataModel>.SetOpetationData(_cacheUrl, cacheKey, operationalData, DateTime.Now.AddMinutes(10));

                }

                var screenDesignHelper = new ScreenDesignHelper(Configuration, _indicator, _cacheUrl);

                var screen = screenDesignHelper.GenerateMenuScreen();
                var display = screenDesignHelper.GenerateDisplayByGivenScreen(screen, operationalData);
                StateFactory<OperationDataModel>.AddOrUpdateStateMachineState(_cacheUrl, cacheKey, new StateModel { DoNotCall = false, Request = null, Response = display, ScreenId = "MenuScreen" }, true, DateTime.Now.AddMinutes(10));

                return display;
            }
            catch (ScreenException ex)
            {
                loggerManager.LogInformation(new StateLog(userName: operationalData.Username, inputParameters: JsonConvert.SerializeObject(model), methodName: nameof(MenuScreen), deviceId: model.device.id, message: ex.ToString()));
                if (ex.State.Equals("screentimeout"))
                    return ScreenDesignHelper.CreateException(ex.State, ex.Header, ex.ErrorMessage, ButtonTypes.exception, $"/{Constants.ServicePath}/device/DeviceCall");
                return ScreenDesignHelper.CreateException(ex.State, ex.Header, ex.ErrorMessage, ButtonTypes.exception, $"/{Constants.ServicePath}/device/MenuScreen");
            }
        }

        [HttpPost]
        [Route("CheckBarcode")]
        public Display CheckBarcode([FromBody] DeviceRequest model)
        {
            var cacheKey = CommonHelper.GenerateCacheKey(model.device.id);
            OperationDataModel operationalData = StateFactory<OperationDataModel>.GetOpetationData(_cacheUrl, cacheKey);
            try
            {
                this.CheckOperationalData(operationalData, model);
                this.SetUserInfoToRequestHeader(operationalData);

                var screenDesignHelper = new ScreenDesignHelper(Configuration, _indicator, _cacheUrl);

                var screen = screenDesignHelper.GenerateCheckBarcodeScreen();
                var display = screenDesignHelper.GenerateDisplayByGivenScreen(screen, operationalData);
                StateFactory<OperationDataModel>.AddOrUpdateStateMachineState(_cacheUrl, cacheKey, new StateModel { DoNotCall = false, Request = null, Response = display, ScreenId = "CheckBarcode" }, true, DateTime.Now.AddMinutes(10));

                return display;
            }
            catch (ScreenException ex)
            {
                loggerManager.LogInformation(new StateLog(userName: operationalData.Username, inputParameters: JsonConvert.SerializeObject(model), methodName: nameof(CheckBarcode), deviceId: model.device.id, message: ex.ToString()));
                if (ex.State.Equals("screentimeout"))
                    return ScreenDesignHelper.CreateException(ex.State, ex.Header, ex.ErrorMessage, ButtonTypes.exception, $"/{Constants.ServicePath}/device/DeviceCall");
                return ScreenDesignHelper.CreateException(ex.State, ex.Header, ex.ErrorMessage, ButtonTypes.exception, $"/{Constants.ServicePath}/device/MenuScreen");
            }
        }

        [HttpPost]
        [Route("BarcodeDetails")]
        public Display BarcodeDetails([FromBody] DeviceRequest model)
        {
            var cacheKey = CommonHelper.GenerateCacheKey(model.device.id);
            OperationDataModel operationalData = StateFactory<OperationDataModel>.GetOpetationData(_cacheUrl, cacheKey);
            this.CheckOperationalData(operationalData, model);
            this.SetUserInfoToRequestHeader(operationalData);
            try
            {
                loggerManager.LogInformation(new StateLog(userName: operationalData.Username, inputParameters: JsonConvert.SerializeObject(model), methodName: nameof(BarcodeDetails), deviceId: model.device.id));

                var screenDesignHelper = new ScreenDesignHelper(Configuration, _indicator, _cacheUrl);

                if (model.data.input[0].value != null)
                {
                    var splittedData = model.data.input[0].value.Split(';');

                    // first value in splitted data is bbData id
                    operationalData.SelectedBBId = splittedData[0];
                }

                var convertedData = 0;

                var result = int.TryParse(operationalData.SelectedBBId, out convertedData);

                if (!result)
                    throw new ScreenException(model.device.id, "BarcodeDetails", "Hatalı Barkod", "UYARI", ButtonTypes.warning, ExceptionTypes.EmptyData);

                operationalData.BBNumberDetails = bBDataervice.Get(item => item.Id == convertedData, true, "BBDetails", "Materials", "Cushions");

                if (operationalData.BBNumberDetails == null)
                    throw new ScreenException(model.device.id, "BarcodeDetails", "Ürün bulunamadı", "UYARI", ButtonTypes.warning, ExceptionTypes.EmptyData);

                StateFactory<OperationDataModel>.SetOpetationData(_cacheUrl, cacheKey, operationalData, DateTime.Now.AddMinutes(10));


                var screen = screenDesignHelper.GenerateBarcodeDetailsScreen(operationalData.BBNumberDetails);
                var display = screenDesignHelper.GenerateDisplayByGivenScreen(screen, operationalData);
                StateFactory<OperationDataModel>.AddOrUpdateStateMachineState(_cacheUrl, cacheKey, new StateModel { DoNotCall = false, Request = null, Response = display, ScreenId = "BarcodeDetails" }, true, DateTime.Now.AddMinutes(10));

                return display;

            }
            catch (ScreenException ex)
            {
                loggerManager.LogInformation(new StateLog(userName: operationalData.Username, inputParameters: JsonConvert.SerializeObject(model), methodName: nameof(BarcodeDetails), deviceId: model.device.id, message: ex.ToString()));
                if (ex.State.Equals("screentimeout"))
                    return ScreenDesignHelper.CreateException(ex.State, ex.Header, ex.ErrorMessage, ButtonTypes.exception, $"/{Constants.ServicePath}/device/DeviceCall");
                return ScreenDesignHelper.CreateException(ex.State, ex.Header, ex.ErrorMessage, ButtonTypes.exception, $"/{Constants.ServicePath}/device/CheckBarcode");
            }
            catch (Exception ex)
            {
                loggerManager.LogInformation(new StateLog(userName: operationalData.Username, inputParameters: JsonConvert.SerializeObject(model), methodName: nameof(BarcodeDetails), deviceId: model.device.id, message: ex.ToString()));
                return ScreenDesignHelper.CreateException("", "HATA", ex.Message, ButtonTypes.exception, $"/{Constants.ServicePath}/device/CheckBarcode");
            }
        }

        [HttpPost]
        [Route("MaterialList")]
        public Display MaterialList([FromBody] DeviceRequest model)
        {
            var cacheKey = CommonHelper.GenerateCacheKey(model.device.id);
            OperationDataModel operationalData = StateFactory<OperationDataModel>.GetOpetationData(_cacheUrl, cacheKey);
            this.CheckOperationalData(operationalData, model);
            this.SetUserInfoToRequestHeader(operationalData);

            try
            {
                operationalData.MaterialsPageNumber = 1;

                if (model.data.@event.Contains("nextPage") || model.data.@event.Contains("prevPage"))
                {
                    operationalData.MaterialsPageNumber = Convert.ToInt32(model.data.@event.Split("_")[1]);

                    StateFactory<OperationDataModel>.SetOpetationData(_cacheUrl, cacheKey, operationalData, DateTime.Now.AddMinutes(10));
                }

                var screenDesignHelper = new ScreenDesignHelper(Configuration, _indicator, _cacheUrl);
                StateFactory<OperationDataModel>.SetOpetationData(_cacheUrl, cacheKey, operationalData, DateTime.Now.AddMinutes(10));

                if (!(operationalData.BBNumberDetails.Materials.Count > 0))
                    throw new ScreenException(model.device.id, "BarcodeDetails", "Detay listesi bulunamadı", "UYARI", ButtonTypes.warning, ExceptionTypes.EmptyData);

                var screen = screenDesignHelper.GenerateMaterialListScreen(operationalData.BBNumberDetails.Materials.ToList(), operationalData.MaterialsPageNumber);
                var display = screenDesignHelper.GenerateDisplayByGivenScreen(screen, operationalData);
                StateFactory<OperationDataModel>.AddOrUpdateStateMachineState(_cacheUrl, cacheKey, new StateModel { DoNotCall = false, Request = null, Response = display, ScreenId = "MaterialList" }, true, DateTime.Now.AddMinutes(10));

                return display;

            }
            catch (ScreenException ex)
            {
                loggerManager.LogInformation(new StateLog(userName: operationalData.Username, inputParameters: JsonConvert.SerializeObject(model), methodName: nameof(MaterialList), deviceId: model.device.id, message: ex.ToString()));

                if (ex.State.Equals("screentimeout"))
                    return ScreenDesignHelper.CreateException(ex.State, ex.Header, ex.ErrorMessage, ButtonTypes.exception, $"/{Constants.ServicePath}/device/DeviceCall");
                return ScreenDesignHelper.CreateException(ex.State, ex.Header, ex.ErrorMessage, ButtonTypes.exception, $"/{Constants.ServicePath}/device/BarcodeDetails");
            }
            catch (Exception ex)
            {
                loggerManager.LogInformation(new StateLog(userName: operationalData.Username, inputParameters: JsonConvert.SerializeObject(model), methodName: nameof(BarcodeDetails), deviceId: model.device.id, message: ex.ToString()));
                return ScreenDesignHelper.CreateException("", "HATA", ex.Message, ButtonTypes.exception, $"/{Constants.ServicePath}/device/CheckBarcode");
            }
        }

        [HttpPost]
        [Route("CushionList")]
        public Display CushionList([FromBody] DeviceRequest model)
        {
            var cacheKey = CommonHelper.GenerateCacheKey(model.device.id);
            OperationDataModel operationalData = StateFactory<OperationDataModel>.GetOpetationData(_cacheUrl, cacheKey);
            this.CheckOperationalData(operationalData, model);
            this.SetUserInfoToRequestHeader(operationalData);

            try
            {
                operationalData.CushionsPageNumber = 1;

                if (model.data.@event.Contains("nextPage") || model.data.@event.Contains("prevPage"))
                {
                    operationalData.CushionsPageNumber = Convert.ToInt32(model.data.@event.Split("_")[1]);

                    StateFactory<OperationDataModel>.SetOpetationData(_cacheUrl, cacheKey, operationalData, DateTime.Now.AddMinutes(10));
                }

                var screenDesignHelper = new ScreenDesignHelper(Configuration, _indicator, _cacheUrl);

                StateFactory<OperationDataModel>.SetOpetationData(_cacheUrl, cacheKey, operationalData, DateTime.Now.AddMinutes(10));

                if (!(operationalData.BBNumberDetails.Cushions.Count > 0))
                    throw new ScreenException(model.device.id, "BarcodeDetails", "Detay listesi bulunamadı", "UYARI", ButtonTypes.warning, ExceptionTypes.EmptyData);

                var screen = screenDesignHelper.GenerateCushionListScreen(operationalData.BBNumberDetails.Cushions.ToList(), operationalData.CushionsPageNumber);
                var display = screenDesignHelper.GenerateDisplayByGivenScreen(screen, operationalData);
                StateFactory<OperationDataModel>.AddOrUpdateStateMachineState(_cacheUrl, cacheKey, new StateModel { DoNotCall = false, Request = null, Response = display, ScreenId = "CushionList" }, true, DateTime.Now.AddMinutes(10));

                return display;

            }
            catch (ScreenException ex)
            {
                loggerManager.LogInformation(new StateLog(userName: operationalData.Username, inputParameters: JsonConvert.SerializeObject(model), methodName: nameof(CushionList), deviceId: model.device.id, message: ex.ToString()));

                if (ex.State.Equals("screentimeout"))
                    return ScreenDesignHelper.CreateException(ex.State, ex.Header, ex.ErrorMessage, ButtonTypes.exception, $"/{Constants.ServicePath}/device/DeviceCall");
                return ScreenDesignHelper.CreateException(ex.State, ex.Header, ex.ErrorMessage, ButtonTypes.exception, $"/{Constants.ServicePath}/device/BarcodeDetails");
            }
            catch (Exception ex)
            {
                loggerManager.LogInformation(new StateLog(userName: operationalData.Username, inputParameters: JsonConvert.SerializeObject(model), methodName: nameof(BarcodeDetails), deviceId: model.device.id, message: ex.ToString()));
                return ScreenDesignHelper.CreateException("", "HATA", ex.Message, ButtonTypes.exception, $"/{Constants.ServicePath}/device/CheckBarcode");
            }
        }

        [HttpPost]
        [Route("CheckTransport")]
        public Display CheckTransport([FromBody] DeviceRequest model)
        {
            var cacheKey = CommonHelper.GenerateCacheKey(model.device.id);
            OperationDataModel operationalData = StateFactory<OperationDataModel>.GetOpetationData(_cacheUrl, cacheKey);
            this.CheckOperationalData(operationalData, model);
            this.SetUserInfoToRequestHeader(operationalData);
            try
            {
                var screenDesignHelper = new ScreenDesignHelper(Configuration, _indicator, _cacheUrl);
                var screen = screenDesignHelper.GenerateCheckTransportScreen();
                var display = screenDesignHelper.GenerateDisplayByGivenScreen(screen, operationalData);
                StateFactory<OperationDataModel>.AddOrUpdateStateMachineState(_cacheUrl, cacheKey, new StateModel { DoNotCall = false, Request = null, Response = display, ScreenId = "CheckTransport" }, true, DateTime.Now.AddMinutes(10));

                return display;

            }
            catch (ScreenException ex)
            {
                loggerManager.LogInformation(new StateLog(userName: operationalData.Username, inputParameters: JsonConvert.SerializeObject(model), methodName: nameof(CheckTransport), deviceId: model.device.id, message: ex.ToString()));

                if (ex.State.Equals("screentimeout"))
                    return ScreenDesignHelper.CreateException(ex.State, ex.Header, ex.ErrorMessage, ButtonTypes.exception, $"/{Constants.ServicePath}/device/DeviceCall");
                return ScreenDesignHelper.CreateException(ex.State, ex.Header, ex.ErrorMessage, ButtonTypes.exception, $"/{Constants.ServicePath}/device/MenuScreen");
            }
            catch (Exception ex)
            {
                loggerManager.LogInformation(new StateLog(userName: operationalData.Username, inputParameters: JsonConvert.SerializeObject(model), methodName: nameof(CheckTransport), deviceId: model.device.id, message: ex.ToString()));

                return ScreenDesignHelper.CreateException("", "HATA", ex.Message, ButtonTypes.exception, $"/{Constants.ServicePath}/device/MenuScreen");
            }
        }

        [HttpPost]
        [Route("CheckTransportResult")]
        public Display CheckTransportResult([FromBody] DeviceRequest model)
        {
            var cacheKey = CommonHelper.GenerateCacheKey(model.device.id);
            OperationDataModel operationalData = StateFactory<OperationDataModel>.GetOpetationData(_cacheUrl, cacheKey);
            this.CheckOperationalData(operationalData, model);
            this.SetUserInfoToRequestHeader(operationalData);
            try
            {
                loggerManager.LogInformation(new StateLog(userName: operationalData.Username, inputParameters: JsonConvert.SerializeObject(model), methodName: nameof(CheckTransportResult), deviceId: model.device.id));

                var screenDesignHelper = new ScreenDesignHelper(Configuration, _indicator, _cacheUrl);

                if (string.IsNullOrEmpty(model.data.input[0].value) || string.IsNullOrEmpty(model.data.input[1].value))
                    throw new ScreenException(model.device.id, "Image", "Değer boş olamaz.", "UYARI", ButtonTypes.warning, ExceptionTypes.EmptyData);

                operationalData.PairingHistory = new PairingHistory();

                BbBarcode bbBarcodeData, transportBarcodeData;
                if (!model.data.input[1].value.TryParseJson<BbBarcode>(out bbBarcodeData))
                    operationalData.PairingHistory.BBNumber = model.data.input[1].value;
                else
                    operationalData.PairingHistory.BBNumber = bbBarcodeData.BBNR;

                if (!model.data.input[0].value.TryParseJson<BbBarcode>(out transportBarcodeData))
                    operationalData.PairingHistory.TransportNumber = model.data.input[0].value;
                else
                    operationalData.PairingHistory.TransportNumber = bbBarcodeData.BBNR;


                loggerManager.LogInformation(new StateLog(userName: operationalData.Username, inputParameters: JsonConvert.SerializeObject(operationalData.PairingHistory), methodName: nameof(CheckTransportResult), deviceId: model.device.id));

                var historyData = pairingHistoryService.Get(p => p.BBNumber == operationalData.PairingHistory.BBNumber);

                loggerManager.LogInformation(new StateLog(userName: operationalData.Username, outputParameters: JsonConvert.SerializeObject(historyData), methodName: nameof(CheckTransportResult), deviceId: model.device.id));

                if (historyData != null)
                    throw new ScreenException(model.device.id, "Image", "BB Numarası birden fazla eşleştirmede kullanılamaz.", "UYARI", ButtonTypes.warning, ExceptionTypes.EmptyData);

                if (operationalData.PairingHistory.TransportNumber != operationalData.PairingHistory.BBNumber)
                {
                    var emailBody = new StringBuilder();
                    emailBody.AppendLine("<p>Merhaba,</p>");
                    emailBody.AppendLine($"<p>{DateTime.Now.ToString("dd/MM/yyyy")} tarihinde {operationalData.PairingHistory.TransportNumber} no.lu araç ile {operationalData.PairingHistory.BBNumber} no.lu araç {operationalData.Username} kullanıcısı tarafından eşleştirilmiştir..</p>");
                    emailBody.AppendLine("<p>Kontrolünün sağlanması ve gerekli değişikliklerin yapılması adına bilgilerinize.</p>");
                    emailBody.AppendLine("<p>İlgili rapora aşağıda yer alan link üzerinden ulaşabilirsiniz.</p>");
                    emailBody.AppendLine($"<p><a href={Configuration["UIUrl"]}>Rapor Link</a></p>");

                    _mailService.SendEmail(new MailRequest(emailBody.ToString()));

                    loggerManager.LogInformation(new StateLog(operationalData.Username, methodName: "MailInformation", inputParameters: $"{operationalData.PairingHistory.TransportNumber} - {operationalData.PairingHistory.BBNumber}"));

                    StateFactory<OperationDataModel>.SetOpetationData(_cacheUrl, cacheKey, operationalData, DateTime.Now.AddMinutes(10));
                    return screenDesignHelper.CreateWarningForCheckTransportConfirmationConfirmation();
                }

                operationalData.PairingHistory.Status = (int)GlobalEnums.PairingHistoryStatusCode.CorrectPairing;

                StateFactory<OperationDataModel>.SetOpetationData(_cacheUrl, cacheKey, operationalData, DateTime.Now.AddMinutes(10));

                pairingHistoryService.Add(operationalData.PairingHistory);

                var screen = screenDesignHelper.GenerateCheckTransportScreen();
                var display = screenDesignHelper.GenerateDisplayByGivenScreen(screen, operationalData);
                StateFactory<OperationDataModel>.AddOrUpdateStateMachineState(_cacheUrl, cacheKey, new StateModel { DoNotCall = false, Request = null, Response = display, ScreenId = "CheckTransportResult" }, true, DateTime.Now.AddMinutes(10));

                return display;

            }
            catch (ScreenException ex)
            {
                loggerManager.LogInformation(new StateLog(userName: operationalData.Username, inputParameters: JsonConvert.SerializeObject(model), methodName: nameof(CheckTransportResult), deviceId: model.device.id, message: ex.ToString()));

                loggerManager.LogInformation($"{ex.Message}");
                if (ex.State.Equals("screentimeout"))
                    return ScreenDesignHelper.CreateException(ex.State, ex.Header, ex.ErrorMessage, ButtonTypes.exception, $"/{Constants.ServicePath}/device/DeviceCall");
                return ScreenDesignHelper.CreateException(ex.State, ex.Header, ex.ErrorMessage, ButtonTypes.exception, $"/{Constants.ServicePath}/device/CheckTransport");
            }
            catch (Exception ex)
            {
                loggerManager.LogInformation(new StateLog(userName: operationalData.Username, inputParameters: JsonConvert.SerializeObject(model), methodName: nameof(CheckTransportResult), deviceId: model.device.id, message: ex.ToString()));

                loggerManager.LogInformation($"{ex.Message}");
                return ScreenDesignHelper.CreateException("", "HATA", ex.Message, ButtonTypes.exception, $"/{Constants.ServicePath}/device/CheckTransport");
            }
        }

        [HttpPost]
        [Route("CheckTransportConfirmation")]
        public Display CheckTransportConfirmation([FromBody] DeviceRequest model)
        {
            var cacheKey = CommonHelper.GenerateCacheKey(model.device.id);
            OperationDataModel operationalData = StateFactory<OperationDataModel>.GetOpetationData(_cacheUrl, cacheKey);
            this.CheckOperationalData(operationalData, model);
            this.SetUserInfoToRequestHeader(operationalData);
            try
            {
                var screenDesignHelper = new ScreenDesignHelper(Configuration, _indicator, _cacheUrl);

                operationalData.PairingHistory.Status = (int)GlobalEnums.PairingHistoryStatusCode.UserApproved;

                StateFactory<OperationDataModel>.SetOpetationData(_cacheUrl, cacheKey, operationalData, DateTime.Now.AddMinutes(10));

                pairingHistoryService.Add(operationalData.PairingHistory);

                var screen = screenDesignHelper.GenerateCheckTransportScreen();
                var display = screenDesignHelper.GenerateDisplayByGivenScreen(screen, operationalData);
                StateFactory<OperationDataModel>.AddOrUpdateStateMachineState(_cacheUrl, cacheKey, new StateModel { DoNotCall = false, Request = null, Response = display, ScreenId = "CheckTransportConfirmation" }, true, DateTime.Now.AddMinutes(10));

                return display;

            }
            catch (ScreenException ex)
            {
                loggerManager.LogInformation(new StateLog(userName: operationalData.Username, inputParameters: JsonConvert.SerializeObject(model), methodName: nameof(CheckTransportConfirmation), deviceId: model.device.id, message: ex.ToString()));

                loggerManager.LogInformation($"{ex.Message}");
                if (ex.State.Equals("screentimeout"))
                    return ScreenDesignHelper.CreateException(ex.State, ex.Header, ex.ErrorMessage, ButtonTypes.exception, $"/{Constants.ServicePath}/device/DeviceCall");
                return ScreenDesignHelper.CreateException(ex.State, ex.Header, ex.ErrorMessage, ButtonTypes.exception, $"/{Constants.ServicePath}/device/CheckTransport");
            }
            catch (Exception ex)
            {
                loggerManager.LogInformation(new StateLog(userName: operationalData.Username, inputParameters: JsonConvert.SerializeObject(model), methodName: nameof(CheckTransportConfirmation), deviceId: model.device.id, message: ex.ToString()));

                loggerManager.LogInformation($"{ex.Message}");
                return ScreenDesignHelper.CreateException("", "HATA", ex.Message, ButtonTypes.exception, $"/{Constants.ServicePath}/device/CheckTransport");
            }
        }

        [HttpPost]
        [Route("ErrorEntry")]
        public Display ErrorEntry([FromBody] DeviceRequest model)
        {
            var cacheKey = CommonHelper.GenerateCacheKey(model.device.id);
            OperationDataModel operationalData = StateFactory<OperationDataModel>.GetOpetationData(_cacheUrl, cacheKey);
            try
            {
                this.CheckOperationalData(operationalData, model);
                this.SetUserInfoToRequestHeader(operationalData);

                var screenDesignHelper = new ScreenDesignHelper(Configuration, _indicator, _cacheUrl);

                //-----------
                var screen = screenDesignHelper.GenerateErrorEntryScreen();
                var display = screenDesignHelper.GenerateDisplayByGivenScreen(screen, operationalData);
                StateFactory<OperationDataModel>.AddOrUpdateStateMachineState(_cacheUrl, cacheKey, new StateModel { DoNotCall = false, Request = null, Response = display, ScreenId = "ErrorEntry" }, true, DateTime.Now.AddMinutes(10));

                return display;

            }
            catch (ScreenException ex)
            {
                loggerManager.LogInformation(new StateLog(userName: operationalData.Username, inputParameters: JsonConvert.SerializeObject(model), methodName: nameof(ErrorEntry), deviceId: model.device.id, message: ex.ToString()));

                if (ex.State.Equals("screentimeout"))
                    return ScreenDesignHelper.CreateException(ex.State, ex.Header, ex.ErrorMessage, ButtonTypes.exception, $"/{Constants.ServicePath}/device/DeviceCall");
                return ScreenDesignHelper.CreateException(ex.State, ex.Header, ex.ErrorMessage, ButtonTypes.exception, $"/{Constants.ServicePath}/device/MenuScreen");
            }
            catch (Exception ex)
            {
                loggerManager.LogInformation(new StateLog(userName: operationalData.Username, inputParameters: JsonConvert.SerializeObject(model), methodName: nameof(BarcodeDetails), deviceId: model.device.id, message: ex.ToString()));
                return ScreenDesignHelper.CreateException("", "HATA", ex.Message, ButtonTypes.exception, $"/{Constants.ServicePath}/device/CheckBarcode");
            }
        }

        [HttpPost]
        [Route("ErrorEntryStatusSelection")]
        public Display ErrorEntryStatusSelection([FromBody] DeviceRequest model)
        {
            var cacheKey = CommonHelper.GenerateCacheKey(model.device.id);
            OperationDataModel operationalData = StateFactory<OperationDataModel>.GetOpetationData(_cacheUrl, cacheKey);
            try
            {
                this.CheckOperationalData(operationalData, model);
                this.SetUserInfoToRequestHeader(operationalData);

                loggerManager.LogInformation(new StateLog(userName: operationalData.Username, inputParameters: JsonConvert.SerializeObject(model), methodName: nameof(ErrorEntryStatusSelection), deviceId: model.device.id));

                if (model.data.input[0].value != null)
                {
                    var splittedData = model.data.input[0].value.Split(';');

                    if (splittedData.Length == 1)
                        throw new ScreenException(model.device.id, "BarcodeDetails", "Hatalı Barkod", "UYARI", ButtonTypes.warning, ExceptionTypes.EmptyData);
                    // first value in splitted data is bbData id
                    operationalData.FaultyBBId = splittedData[0];
                    // second value in splitted data is bbData id
                    operationalData.FaultySeatId = splittedData[1];
                }

                loggerManager.LogInformation(new StateLog(userName: operationalData.Username, message: $"FaultyBBId {operationalData.FaultyBBId} FaultySeatId {operationalData.FaultySeatId}", methodName: nameof(ErrorEntryStatusSelection), deviceId: model.device.id));

                var bBNumber = bBDataervice.Get(item => item.Id == Convert.ToInt32(operationalData.FaultyBBId));
                if (bBNumber == null)
                    throw new ScreenException(model.device.id, "Image", "Ürün bulunamadı", "UYARI", ButtonTypes.warning, ExceptionTypes.EmptyData);

                StateFactory<OperationDataModel>.SetOpetationData(_cacheUrl, cacheKey, operationalData, DateTime.Now.AddMinutes(10));

                var screenDesignHelper = new ScreenDesignHelper(Configuration, _indicator, _cacheUrl);

                //-----------
                var screen = screenDesignHelper.GenerateErrorEntryStatusSelectionScreen();
                var display = screenDesignHelper.GenerateDisplayByGivenScreen(screen, operationalData);
                StateFactory<OperationDataModel>.AddOrUpdateStateMachineState(_cacheUrl, cacheKey, new StateModel { DoNotCall = false, Request = null, Response = display, ScreenId = "ErrorEntry" }, true, DateTime.Now.AddMinutes(10));

                return display;

            }
            catch (ScreenException ex)
            {
                loggerManager.LogInformation(new StateLog(userName: operationalData.Username, inputParameters: JsonConvert.SerializeObject(model), methodName: nameof(ErrorEntryStatusSelection), deviceId: model.device.id, message: ex.ToString()));

                if (ex.State.Equals("screentimeout"))
                    return ScreenDesignHelper.CreateException(ex.State, ex.Header, ex.ErrorMessage, ButtonTypes.exception, $"/{Constants.ServicePath}/device/DeviceCall");
                return ScreenDesignHelper.CreateException(ex.State, ex.Header, ex.ErrorMessage, ButtonTypes.exception, $"/{Constants.ServicePath}/device/ErrorEntry");
            }
            catch (Exception ex)
            {
                loggerManager.LogInformation(new StateLog(userName: operationalData.Username, inputParameters: JsonConvert.SerializeObject(model), methodName: nameof(BarcodeDetails), deviceId: model.device.id, message: ex.ToString()));
                return ScreenDesignHelper.CreateException("", "HATA", ex.Message, ButtonTypes.exception, $"/{Constants.ServicePath}/device/CheckBarcode");
            }
        }

        [HttpPost]
        [Route("ErrorEntryImage")]
        public Display ErrorEntryImage([FromBody] DeviceRequest model)
        {
            var cacheKey = CommonHelper.GenerateCacheKey(model.device.id);
            OperationDataModel operationalData = StateFactory<OperationDataModel>.GetOpetationData(_cacheUrl, cacheKey);
            try
            {
                this.CheckOperationalData(operationalData, model);
                this.SetUserInfoToRequestHeader(operationalData);

                var screenDesignHelper = new ScreenDesignHelper(Configuration, _indicator, _cacheUrl);

                operationalData.FaultyBBStatus = Convert.ToInt32(model.data.@event);

                StateFactory<OperationDataModel>.SetOpetationData(_cacheUrl, cacheKey, operationalData, DateTime.Now.AddMinutes(10));

                var screen = screenDesignHelper.GenerateErrorEntryImageScreen();
                var display = screenDesignHelper.GenerateCaptureDisplayByGivenScreen(screen, operationalData);
                StateFactory<OperationDataModel>.AddOrUpdateStateMachineState(_cacheUrl, cacheKey, new StateModel { DoNotCall = false, Request = null, Response = display, ScreenId = "ErrorEntryImage" }, true, DateTime.Now.AddMinutes(10));

                return display;

            }
            catch (ScreenException ex)
            {
                loggerManager.LogInformation(new StateLog(userName: operationalData.Username, inputParameters: JsonConvert.SerializeObject(model), methodName: nameof(ErrorEntryImage), deviceId: model.device.id, message: ex.ToString()));

                if (ex.State.Equals("screentimeout"))
                    return ScreenDesignHelper.CreateException(ex.State, ex.Header, ex.ErrorMessage, ButtonTypes.exception, $"/{Constants.ServicePath}/device/DeviceCall");
                return ScreenDesignHelper.CreateException(ex.State, ex.Header, ex.ErrorMessage, ButtonTypes.exception, $"/{Constants.ServicePath}/device/ErrorEntry");
            }
            catch (Exception ex)
            {
                loggerManager.LogInformation(new StateLog(userName: operationalData.Username, inputParameters: JsonConvert.SerializeObject(model), methodName: nameof(ErrorEntryImage), deviceId: model.device.id, message: ex.ToString()));

                return ScreenDesignHelper.CreateException("", "HATA", ex.Message, ButtonTypes.exception, $"/{Constants.ServicePath}/device/ErrorEntry");
            }
        }

        // devicecall(configuration url) is default route for httpput request
        [HttpPut]
        [DisableRequestSizeLimit]
        [Route("DeviceCall")]
        public Display DeviceCall(System.Threading.CancellationToken cancellationToken)
        {
            var deviceId = Request.Headers["Cookie"].ToString().Split(';')[0].Replace("id=", "");
            loggerManager.LogInformation(new StateLog(userName: "", methodName: nameof(DeviceCall), deviceId: deviceId, message: "PUT"));

            var cacheKey = CommonHelper.GenerateCacheKey(deviceId);
            OperationDataModel operationalData = StateFactory<OperationDataModel>.GetOpetationData(_cacheUrl, cacheKey);
            this.CheckOperationalData(operationalData, new DeviceRequest { device = new Device { id = deviceId } });
            this.SetUserInfoToRequestHeader(operationalData);

            try
            {
                var encode = Request.Headers["Transfer-Encoding"].ToString();

                using (var inputStream = new MemoryStream())
                {
                    //-----------
                    Request.Body.CopyToAsync(inputStream).Wait(cancellationToken);

                    var fullPath = Path.Combine(_env.ContentRootPath, "FaultyImages");

                    loggerManager.LogInformation(new StateLog(userName: operationalData.Username, methodName: nameof(DeviceCall), deviceId: deviceId, message: $"PUT - {fullPath}"));

                    if (!Directory.Exists(fullPath)) Directory.CreateDirectory(fullPath);

                    var img = Image.FromStream(inputStream);

                    var bBData = bBDataervice.Get(item => item.Id == Convert.ToInt32(operationalData.FaultyBBId), true, Constants.BBNumberRelations.BBDetails, Constants.BBNumberRelations.Seats);
                    if (bBData == null)
                        throw new ScreenException(deviceId, "Image", "Ürün bulunamadı", "UYARI", ButtonTypes.warning, ExceptionTypes.EmptyData);

                    var imgName = $"{bBData.BBNumber} - {Guid.NewGuid().ToString()}.jpg";

                    img.Save($"{fullPath}\\{imgName}", ImageFormat.Jpeg);

                    operationalData.ImgName = imgName;

                    StateFactory<OperationDataModel>.SetOpetationData(_cacheUrl, cacheKey, operationalData, DateTime.Now.AddMinutes(10));

                    loggerManager.LogInformation(new StateLog(userName: operationalData.Username, methodName: nameof(DeviceCall), deviceId: deviceId, message: "PUT"));

                }

                var screenDesignHelper = new ScreenDesignHelper(Configuration, _indicator, _cacheUrl);

                var screen = screenDesignHelper.GenerateErrorEntryReproductionConfirmationScreen();
                var display = screenDesignHelper.GenerateDisplayByGivenScreen(screen, operationalData);
                StateFactory<OperationDataModel>.AddOrUpdateStateMachineState(_cacheUrl, cacheKey, new StateModel { DoNotCall = false, Request = null, Response = display, ScreenId = "ErrorEntry" }, true, DateTime.Now.AddMinutes(10));

                return display;

            }
            catch (ScreenException ex)
            {
                loggerManager.LogInformation(new StateLog(userName: operationalData.Username, methodName: nameof(DeviceCall), deviceId: deviceId, message: ex.ToString()));

                if (ex.State.Equals("screentimeout"))
                    return ScreenDesignHelper.CreateException(ex.State, ex.Header, ex.ErrorMessage, ButtonTypes.exception, $"/{Constants.ServicePath}/device/DeviceCall");

                return ScreenDesignHelper.CreateException(ex.State, ex.Header, ex.ErrorMessage, ButtonTypes.exception, $"/{Constants.ServicePath}/device/ErrorEntryImage");
            }
            catch (Exception ex)
            {
                loggerManager.LogInformation(new StateLog(userName: operationalData.Username, methodName: nameof(DeviceCall), deviceId: deviceId, message: ex.ToString()));

                return ScreenDesignHelper.CreateException("", "HATA", ex.Message, ButtonTypes.exception, $"/{Constants.ServicePath}/device/ErrorEntry");
            }
        }


        [HttpPost]
        [Route("ErrorEntryReproductionConfirmation")]
        public Display ErrorEntryReproductionConfirmation([FromBody] DeviceRequest model)
        {
            var cacheKey = CommonHelper.GenerateCacheKey(model.device.id);
            OperationDataModel operationalData = StateFactory<OperationDataModel>.GetOpetationData(_cacheUrl, cacheKey);
            try
            {
                this.CheckOperationalData(operationalData, model);
                this.SetUserInfoToRequestHeader(operationalData);

                var screenDesignHelper = new ScreenDesignHelper(Configuration, _indicator, _cacheUrl);

                operationalData.FaultyBBReProductionStatus = model.data.@event.Equals("btnConfirm");


                var bBData = bBDataervice.Get(item => item.Id == Convert.ToInt32(operationalData.FaultyBBId), true, Constants.BBNumberRelations.BBDetails, Constants.BBNumberRelations.Seats);
                if (bBData == null)
                    throw new ScreenException(model.device.id, "Image", "Ürün bulunamadı", "UYARI", ButtonTypes.warning, ExceptionTypes.EmptyData);

                var faultyBB = new FaultyBB
                {
                    SeatId = Convert.ToInt32(operationalData.FaultySeatId),
                    BBDataId = Convert.ToInt32(operationalData.FaultyBBId),
                    BBNumber = bBData.BBNumber,
                    Status = operationalData.FaultyBBStatus,
                    ReProductionStatus = operationalData.FaultyBBReProductionStatus,
                    ImgName = operationalData.ImgName,
                    Date = DateTime.Now
                };

                faultyBBService.Add(faultyBB);

                StateFactory<OperationDataModel>.SetOpetationData(_cacheUrl, cacheKey, operationalData, DateTime.Now.AddMinutes(10));

                var screen = screenDesignHelper.GenerateMenuScreen();
                var display = screenDesignHelper.GenerateCaptureDisplayByGivenScreen(screen, operationalData);
                StateFactory<OperationDataModel>.AddOrUpdateStateMachineState(_cacheUrl, cacheKey, new StateModel { DoNotCall = false, Request = null, Response = display, ScreenId = "ErrorEntryImage" }, true, DateTime.Now.AddMinutes(10));

                return display;

            }
            catch (ScreenException ex)
            {
                loggerManager.LogInformation(new StateLog(userName: operationalData.Username, inputParameters: JsonConvert.SerializeObject(model), methodName: nameof(ErrorEntryReproductionConfirmation), deviceId: model.device.id, message: ex.ToString()));

                if (ex.State.Equals("screentimeout"))
                    return ScreenDesignHelper.CreateException(ex.State, ex.Header, ex.ErrorMessage, ButtonTypes.exception, $"/{Constants.ServicePath}/device/DeviceCall");
                return ScreenDesignHelper.CreateException(ex.State, ex.Header, ex.ErrorMessage, ButtonTypes.exception, $"/{Constants.ServicePath}/device/ErrorEntry");
            }
            catch (Exception ex)
            {
                loggerManager.LogInformation(new StateLog(userName: operationalData.Username, inputParameters: JsonConvert.SerializeObject(model), methodName: nameof(ErrorEntryReproductionConfirmation), deviceId: model.device.id, message: ex.ToString()));

                return ScreenDesignHelper.CreateException("", "HATA", ex.Message, ButtonTypes.exception, $"/{Constants.ServicePath}/device/ErrorEntry");
            }
        }

        private void CheckOperationalData(OperationDataModel operationalData, DeviceRequest model)
        {
            if (operationalData == null)
                throw new ScreenException(model.device.id, "screentimeout", "Zaman sınırı aşıldığı için otomaik çıkış yapıldı.", "UYARI", ButtonTypes.warning, ExceptionTypes.EmptyData);

        }

        private void SetUserInfoToRequestHeader(OperationDataModel operationalData)
        {
            //set user name to request header for setting user info to db
            Request.Headers.Add("UserName", operationalData.Username);
        }
    }
}
