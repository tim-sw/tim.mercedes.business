﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TIM.SDK.GLOGI;
using TIM.SDK.GLOGI.Executions;

namespace TIM.MERCEDES.GLOGISERVICE.Helpers
{
    public static class CommonHelper
    {
        public static Indicator DoInitIndicator()
        {
            var buzzer = DisplayFactory.CreateBuzzer(Status.On, 100, 100, 1);
            var light = DisplayFactory.CreateLight(Color.Green, Status.On, 100, 100, 1);
            var vibration = DisplayFactory.CreateVibration(Status.On, 100, 100, 1);
            var indicator = DisplayFactory.InitializeIndicator(light, buzzer, vibration);
            return indicator;
        }

        public static string GenerateCacheKey(string operationId)
        {
            return "MercedesOperation_" + operationId;
        }

        public static bool TryParseJson<T>(this string @this, out T result)
        {
            bool success = true;
            var settings = new JsonSerializerSettings
            {
                Error = (sender, args) => { success = false; args.ErrorContext.Handled = true; },
                MissingMemberHandling = MissingMemberHandling.Error
            };

            result = JsonConvert.DeserializeObject<T>(@this, settings);
            return success;
        }
    }
}
