﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TIM.MERCEDES.COMMON.Enums;
using TIM.MERCEDES.ENTITIES.Concrete;
using TIM.MERCEDES.GLOGISERVICE.Models;
using TIM.SDK.GLOGI;
using TIM.SDK.GLOGI.Executions;

namespace TIM.MERCEDES.GLOGISERVICE.Helpers
{
    public class ScreenDesignHelper
    {
        private Indicator _indicator { get; set; }

        private readonly IConfiguration _configuration;

        private string _cacheKey;

        private string _cacheUrl;

        private string _userName;

        public ScreenDesignHelper(IConfiguration configuration, Indicator indicator, string cacheKey, string userName = null)
        {
            _configuration = configuration;
            _cacheKey = cacheKey;
            _indicator = indicator;
            _userName = userName;
            _cacheUrl = _configuration["CacheUrl"];
        }

        public Screen GenerateLoginScreen()
        {
            var screen = DisplayFactory.InitializeScreen("loginscreen", "Giriş", null, null, $"/{Constants.ServicePath}/device/menuscreen");
            DisplayFactory.AddScreenElement(screen, DisplayFactory.CreateScreenElement(ScreenElementType.Text, null, null, null, "Kullanıcı Adı", null, Font.M, Alignment.Center, null), 1);
            DisplayFactory.AddScreenElement(screen, DisplayFactory.CreateScreenElement(ScreenElementType.Input, "txtusername", null, null, null, null, Font.M, Alignment.Center, null), 2);
            DisplayFactory.AddScreenElement(screen, DisplayFactory.CreateScreenElement(ScreenElementType.Text, null, null, null, "Şifre", null, Font.M, Alignment.Center, null), 3);
            DisplayFactory.AddScreenElement(screen, DisplayFactory.CreateScreenElement(ScreenElementType.Input, "txtpassword", null, null, null, null, Font.M, Alignment.Center, null), 4);

            return screen;
        }

        public Screen GenerateMenuScreen()
        {
            var screen = DisplayFactory.InitializeScreen("menuscreen", "");
            DisplayFactory.AddScreenElement(screen, DisplayFactory.CreateScreenElement(ScreenElementType.Button, "checkBarcode", null, "Barkod Kontrol", null, null, Font.M, Alignment.Center, null, $"/{Constants.ServicePath}/device/CheckBarcode"), 1);
            DisplayFactory.AddScreenElement(screen, DisplayFactory.CreateScreenElement(ScreenElementType.Button, "errorEntry", null, "Hata Girişi", null, null, Font.M, Alignment.Center, null, $"/{Constants.ServicePath}/device/ErrorEntry"), 2);
            DisplayFactory.AddScreenElement(screen, DisplayFactory.CreateScreenElement(ScreenElementType.Button, "checkTransport", null, "Trasport & Otobüs Kontrolü", null, null, Font.M, Alignment.Center, null, $"/{Constants.ServicePath}/device/CheckTransport"), 3);
            DisplayFactory.AddScreenElement(screen, DisplayFactory.CreateScreenElement(ScreenElementType.Button, "exit", null, "Çıkış", null, Functionkey.FN1, Font.S, Alignment.Left, null, $"/{Constants.ServicePath}/device/DeviceCall"), 9);
            return screen;
        }

        public Screen GenerateCheckBarcodeScreen()
        {
            var screen = DisplayFactory.InitializeScreen("checkbarcode", "Barkod Kontrol", null, null, $"/{Constants.ServicePath}/device/BarcodeDetails");
            DisplayFactory.AddScreenElement(screen, DisplayFactory.CreateScreenElement(ScreenElementType.Text, null, null, null, "Barkod: ", null, Font.M, Alignment.Center, null), 1);
            DisplayFactory.AddScreenElement(screen, DisplayFactory.CreateScreenElement(ScreenElementType.Input, "txtBarcode", null, null, null, null, Font.M, Alignment.Center, null), 2);

            DisplayFactory.AddScreenElement(screen, DisplayFactory.CreateScreenElement(ScreenElementType.Button, "btnBack", null, "GERİ", null, Functionkey.FN3, Font.S, Alignment.Left, null, $"/{Constants.ServicePath}/device/MenuScreen"), 9);
            return screen;
        }

        public Screen GenerateBarcodeDetailsScreen(BBData bBNumber)
        {
            var screen = DisplayFactory.InitializeScreen("checkbarcode", "Detay");
            DisplayFactory.AddScreenElement(screen, DisplayFactory.CreateScreenElement(ScreenElementType.Text, null, null, null, "BB No: " + bBNumber.BBNumber, null, Font.S, Alignment.Left, null), 1);
            DisplayFactory.AddScreenElement(screen, DisplayFactory.CreateScreenElement(ScreenElementType.Text, null, null, null, "Açıklama: " + bBNumber.BBDetails.FirstOrDefault()?.Explanation1, null, Font.S, Alignment.Left, null), 2);
            DisplayFactory.AddScreenElement(screen, DisplayFactory.CreateScreenElement(ScreenElementType.Text, null, null, null, "Tip: " + bBNumber.BBDetails.FirstOrDefault()?.SeatType, null, Font.S, Alignment.Left, null), 3);

            DisplayFactory.AddScreenElement(screen, DisplayFactory.CreateScreenElement(ScreenElementType.Button, "btnMaterialList", null, "Kumaş Listesi", null, Functionkey.FN1, Font.S, Alignment.Center, null, $"/{Constants.ServicePath}/device/MaterialList"), 6);
            DisplayFactory.AddScreenElement(screen, DisplayFactory.CreateScreenElement(ScreenElementType.Button, "btnCushionList", null, "Minder Listesi", null, Functionkey.FN2, Font.S, Alignment.Center, null, $"/{Constants.ServicePath}/device/CushionList"), 7);
            DisplayFactory.AddScreenElement(screen, DisplayFactory.CreateScreenElement(ScreenElementType.Button, "btnBack", null, "GERİ", null, Functionkey.FN3, Font.S, Alignment.Left, null, $"/{Constants.ServicePath}/device/CheckBarcode"), 9);
            return screen;
        }

        public Screen GenerateCheckTransportScreen()
        {
            var screen = DisplayFactory.InitializeScreen("checkbarcode", "Detay", url: $"/{Constants.ServicePath}/device/CheckTransportResult");
            DisplayFactory.AddScreenElement(screen, DisplayFactory.CreateScreenElement(ScreenElementType.Text, null, null, null, "Transport Barkod: ", null, Font.M, Alignment.Left, null), 1);
            DisplayFactory.AddScreenElement(screen, DisplayFactory.CreateScreenElement(ScreenElementType.Input, "txtTransportBarcode", null, null, null, null, Font.M, Alignment.Left, null), 2);

            DisplayFactory.AddScreenElement(screen, DisplayFactory.CreateScreenElement(ScreenElementType.Text, null, null, null, "BB Barkod: ", null, Font.M, Alignment.Left, null), 3);
            DisplayFactory.AddScreenElement(screen, DisplayFactory.CreateScreenElement(ScreenElementType.Input, "txtBBBarcode", null, null, null, null, Font.M, Alignment.Left, null), 4);

            DisplayFactory.AddScreenElement(screen, DisplayFactory.CreateScreenElement(ScreenElementType.Button, "btnBack", null, "GERİ", null, Functionkey.FN3, Font.S, Alignment.Left, null, $"/{Constants.ServicePath}/device/MenuScreen"), 8);
            return screen;
        }

        public Screen GenerateCheckTransportConfirmationScreen()
        {
            var screen = DisplayFactory.InitializeScreen("checkbarcode", "Detay");
            DisplayFactory.AddScreenElement(screen, DisplayFactory.CreateScreenElement(ScreenElementType.Text, null, null, null, "Yapılan İşlemi", null, Font.M, Alignment.Center, null), 1);
            DisplayFactory.AddScreenElement(screen, DisplayFactory.CreateScreenElement(ScreenElementType.Text, null, null, null, "Onaylıyor musunuz?", null, Font.M, Alignment.Center, null), 2);
            DisplayFactory.AddScreenElement(screen, DisplayFactory.CreateScreenElement(ScreenElementType.Button, "btnConfirm", null, "Evet", null, null, Font.M, Alignment.Left, null, $"/{Constants.ServicePath}/device/CheckTransportConfirmation"), 4);
            DisplayFactory.AddScreenElement(screen, DisplayFactory.CreateScreenElement(ScreenElementType.Button, "btnReject", null, "Hayır", null, null, Font.M, Alignment.Right, null, $"/{Constants.ServicePath}/device/CheckTransport"), 4);


            DisplayFactory.AddScreenElement(screen, DisplayFactory.CreateScreenElement(ScreenElementType.Button, "btnBack", null, "GERİ", null, Functionkey.FN3, Font.S, Alignment.Left, null, $"/{Constants.ServicePath}/device/CheckTransport"), 9);
            return screen;
        }

        public Screen GenerateMaterialListScreen(List<Material> materials, int pageNumber = 1)
        {
            var screen = DisplayFactory.InitializeScreen("checkbarcode", "Detay", null, null, $"/{Constants.ServicePath}/device/MaterialList");

            var skipIndex = (pageNumber - 1) * 2;

            var finalDataList = materials.Skip(skipIndex).Take(2);

            var itemIndex = 1;
            foreach (var item in finalDataList)
            {
                DisplayFactory.AddScreenElement(screen, DisplayFactory.CreateScreenElement(ScreenElementType.Text, null, null, null, "Grup: " + item.Group, null, Font.S, Alignment.Left, null), itemIndex);
                itemIndex++;
                DisplayFactory.AddScreenElement(screen, DisplayFactory.CreateScreenElement(ScreenElementType.Text, null, null, null, "Materyal: " + item.MaterialNumber, null, Font.XS, Alignment.Left, null), itemIndex);
                itemIndex++;
                DisplayFactory.AddScreenElement(screen, DisplayFactory.CreateScreenElement(ScreenElementType.Text, null, null, null, "Açıklama: " + item.Name, null, Font.S, Alignment.Left, null), itemIndex);
                itemIndex += 2;

            }

            if (materials.Count > 2)
            {
                if (materials.LastOrDefault().Id != finalDataList.LastOrDefault().Id)
                    DisplayFactory.AddScreenElement(screen, DisplayFactory.CreateScreenElement(ScreenElementType.Button, $"nextPage_{pageNumber + 1}", null, ">", null, null, Font.S, Alignment.Right, null, $"/{Constants.ServicePath}/device/MaterialList"), 8);
                if (pageNumber > 1)
                    DisplayFactory.AddScreenElement(screen, DisplayFactory.CreateScreenElement(ScreenElementType.Button, $"prevPage_{pageNumber - 1}", null, "<", null, null, Font.S, Alignment.Left, null, $"/{Constants.ServicePath}/device/MaterialList"), 8);
            }

            DisplayFactory.AddScreenElement(screen, DisplayFactory.CreateScreenElement(ScreenElementType.Button, "btnBack", null, "GERİ", null, Functionkey.FN3, Font.S, Alignment.Left, null, $"/{Constants.ServicePath}/device/BarcodeDetails"), 9);
            return screen;
        }

        public Screen GenerateCushionListScreen(List<Cushion> cushions, int pageNumber = 1)
        {
            var screen = DisplayFactory.InitializeScreen("checkbarcode", "Detay");

            var skipIndex = (pageNumber - 1) * 2;

            var finalDataList = cushions.Skip(skipIndex).Take(2);

            var itemIndex = 1;
            foreach (var item in finalDataList)
            {
                DisplayFactory.AddScreenElement(screen, DisplayFactory.CreateScreenElement(ScreenElementType.Text, null, null, null, "Ad: " + item.MaterialName, null, Font.S, Alignment.Left, null), itemIndex);
                itemIndex++;
                DisplayFactory.AddScreenElement(screen, DisplayFactory.CreateScreenElement(ScreenElementType.Text, null, null, null, "Materyal: " + item.MaterialNumber, null, Font.XS, Alignment.Left, null), itemIndex);
                itemIndex++;
                DisplayFactory.AddScreenElement(screen, DisplayFactory.CreateScreenElement(ScreenElementType.Text, null, null, null, "Birim: " + item.Unit, null, Font.S, Alignment.Left, null), itemIndex);
                itemIndex += 2;

            }
            if (cushions.Count > 2)
            {
                if(cushions.LastOrDefault().Id != finalDataList.LastOrDefault().Id)
                    DisplayFactory.AddScreenElement(screen, DisplayFactory.CreateScreenElement(ScreenElementType.Button, $"nextPage_{pageNumber + 1}", null, ">", null, null, Font.S, Alignment.Right, null, $"/{Constants.ServicePath}/device/CushionList"), 8);
                if (pageNumber > 1)
                    DisplayFactory.AddScreenElement(screen, DisplayFactory.CreateScreenElement(ScreenElementType.Button, $"prevPage_{pageNumber - 1}", null, "<", null, null, Font.S, Alignment.Left, null, $"/{Constants.ServicePath}/device/CushionList"), 8);
            }

            DisplayFactory.AddScreenElement(screen, DisplayFactory.CreateScreenElement(ScreenElementType.Button, "btnBack", null, "GERİ", null, Functionkey.FN3, Font.S, Alignment.Left, null, $"/{Constants.ServicePath}/device/BarcodeDetails"), 9);
            return screen;
        }

        public Screen GenerateErrorEntryScreen()
        {
            var screen = DisplayFactory.InitializeScreen("checkbarcode", "Detay", null, null, $"/{Constants.ServicePath}/device/ErrorEntryStatusSelection");
            DisplayFactory.AddScreenElement(screen, DisplayFactory.CreateScreenElement(ScreenElementType.Text, null, null, null, "Barkod: ", null, Font.M, Alignment.Left, null), 1);
            DisplayFactory.AddScreenElement(screen, DisplayFactory.CreateScreenElement(ScreenElementType.Input, "txtBarcode", null, null, null, null, Font.M, Alignment.Center, null), 1);

            //DisplayFactory.AddScreenElement(screen, DisplayFactory.CreateScreenElement(ScreenElementType.Text, null, null, null, "Koltuk No: ", null, Font.M, Alignment.Left, null), 3);
            //DisplayFactory.AddScreenElement(screen, DisplayFactory.CreateScreenElement(ScreenElementType.Input, "txtSeatNo", null, null, null, null, Font.M, Alignment.Center, null), 3);

            DisplayFactory.AddScreenElement(screen, DisplayFactory.CreateScreenElement(ScreenElementType.Button, "btnBack", null, "GERİ", null, Functionkey.FN3, Font.S, Alignment.Left, null, $"/{Constants.ServicePath}/device/MenuScreen"), 9);
            return screen;
        }

        public Screen GenerateErrorEntryStatusSelectionScreen()
        {
            var screen = DisplayFactory.InitializeScreen("checkbarcode", "Durum Seçimi", null, null, $"/{Constants.ServicePath}/device/ErrorEntryImage");
            DisplayFactory.AddScreenElement(screen, DisplayFactory.CreateScreenElement(ScreenElementType.Button, ((int)GlobalEnums.FaultyBBDataStatusCode.Faulty).ToString(), null, "Hatalı", null, null, Font.M, Alignment.Center, null, $"/{Constants.ServicePath}/device/ErrorEntryImage"), 1);
            DisplayFactory.AddScreenElement(screen, DisplayFactory.CreateScreenElement(ScreenElementType.Button, ((int)GlobalEnums.FaultyBBDataStatusCode.Damaged).ToString(), null, "Hasarlı", null, null, Font.M, Alignment.Center, null, $"/{Constants.ServicePath}/device/ErrorEntryImage"), 2);
            //DisplayFactory.AddScreenElement(screen, DisplayFactory.CreateScreenElement(ScreenElementType.Button, ((int)GlobalEnums.FaultyBBDataStatusCode.ReProduction).ToString(), null, "Yeniden Üretim", null, null, Font.M, Alignment.Center, null, $"/{Constants.ServicePath}/device/ErrorEntryImage"), 3);

            DisplayFactory.AddScreenElement(screen, DisplayFactory.CreateScreenElement(ScreenElementType.Button, "btnBack", null, "GERİ", null, Functionkey.FN3, Font.S, Alignment.Left, null, $"/{Constants.ServicePath}/device/ErrorEntry"), 9);
            return screen;
        }

        public Screen GenerateErrorEntryImageScreen()
        {
            var screen = DisplayFactory.InitializeScreen("checkbarcode", "Detay", url: $"/{Constants.ServicePath}/device/devicecall");

            DisplayFactory.AddScreenElement(screen, DisplayFactory.CreateScreenElement(ScreenElementType.Text, null, null, null, "Resim Okut: ", null, Font.M, Alignment.Left, null), 1);
            DisplayFactory.AddScreenElement(screen, DisplayFactory.CreateScreenElement(ScreenElementType.Input, "txtImg", null, null, null, null, Font.M, Alignment.Center, null), 1);

            DisplayFactory.AddScreenElement(screen, DisplayFactory.CreateScreenElement(ScreenElementType.Button, "btnBack", null, "GERİ", null, Functionkey.FN3, Font.S, Alignment.Left, null, $"/{Constants.ServicePath}/device/ErrorEntryBarCode"), 9);
            return screen;
        }

        public Screen GenerateErrorEntryReproductionConfirmationScreen()
        {
            var screen = DisplayFactory.InitializeScreen("ErrorEntryReproduction", "Detay");
            DisplayFactory.AddScreenElement(screen, DisplayFactory.CreateScreenElement(ScreenElementType.Text, null, null, null, "Yeniden", null, Font.M, Alignment.Center, null), 1);
            DisplayFactory.AddScreenElement(screen, DisplayFactory.CreateScreenElement(ScreenElementType.Text, null, null, null, "Üretilsin mi?", null, Font.M, Alignment.Center, null), 2);
            DisplayFactory.AddScreenElement(screen, DisplayFactory.CreateScreenElement(ScreenElementType.Button, "btnConfirm", null, "Evet", null, null, Font.M, Alignment.Left, null, $"/{Constants.ServicePath}/device/ErrorEntryReproductionConfirmation"), 4);
            DisplayFactory.AddScreenElement(screen, DisplayFactory.CreateScreenElement(ScreenElementType.Button, "btnReject", null, "Hayır", null, null, Font.M, Alignment.Right, null, $"/{Constants.ServicePath}/device/ErrorEntryReproductionConfirmation"), 4);


            //DisplayFactory.AddScreenElement(screen, DisplayFactory.CreateScreenElement(ScreenElementType.Button, "btnBack", null, "GERİ", null, Functionkey.FN3, Font.S, Alignment.Left, null, $"/{Constants.ServicePath}/device/CheckTransport"), 9);
            return screen;
        }

        public static Display CreateWarning(string state, string header, string message, string buttonUrl = null, string buttonName = "OK")
        {
            using (Display display = new Display())
            {
                display.mode = DisplayMode.BarcodeScan;
                display.sleep = 0;
                display.screen = new Screen();
                display.screen.header = header;

                var buzzer = DisplayFactory.CreateBuzzer(Status.On, 1000, 100, 1);
                var light = DisplayFactory.CreateLight(Color.Red, Status.On, 1000, 1000, 1);
                var vibration = DisplayFactory.CreateVibration(Status.On, 1200, 100, 1);

                display.indicator = new Indicator();
                display.indicator.buzzer = buzzer;
                display.indicator.vibration = vibration;
                display.indicator.light = light;
                display.screen.body = new List<List<ScreenElement>>();
                display.screen.state = state;

                int chunkSize = 20;
                int stringLength = message.Length;
                for (int i = 0; i < stringLength; i += chunkSize)
                {
                    if (i + chunkSize > stringLength) chunkSize = stringLength - i;
                    {
                        var elemExceptionText = new List<ScreenElement>();
                        elemExceptionText.Add(new ScreenElement
                        {
                            type = "text",
                            value = message.Substring(i, chunkSize),
                            font_no = 2,
                            align = "left"
                        });
                        display.screen.body.Add(elemExceptionText);

                    }
                }


                var elemExceptionButton = new List<ScreenElement>();
                elemExceptionButton.Add(new ScreenElement
                {
                    id = "btnWarning",
                    type = "button",
                    name = buttonName,
                    font_no = 2,
                    fn = 5,
                    align = "center",
                    url = buttonUrl
                });
                display.screen.body.Add(elemExceptionButton);
                return display;
            }
        }

        public Display CreateWarningForCheckTransportConfirmationConfirmation()
        {

            var screen = DisplayFactory.InitializeScreen("checkbarcode", "UYARI");
            DisplayFactory.AddScreenElement(screen, DisplayFactory.CreateScreenElement(ScreenElementType.Text, null, null, null, "Yapılan İşlemi", null, Font.M, Alignment.Center, null), 1);
            DisplayFactory.AddScreenElement(screen, DisplayFactory.CreateScreenElement(ScreenElementType.Text, null, null, null, "Onaylıyor musunuz?", null, Font.M, Alignment.Center, null), 2);
            DisplayFactory.AddScreenElement(screen, DisplayFactory.CreateScreenElement(ScreenElementType.Button, "btnConfirm", null, "Evet", null, null, Font.M, Alignment.Left, null, $"/{Constants.ServicePath}/device/CheckTransportConfirmation"), 4);
            DisplayFactory.AddScreenElement(screen, DisplayFactory.CreateScreenElement(ScreenElementType.Button, "btnReject", null, "Hayır", null, null, Font.M, Alignment.Right, null, $"/{Constants.ServicePath}/device/CheckTransport"), 4);


            var _indicator = new Indicator
            {
                buzzer = DisplayFactory.CreateBuzzer(Status.On, 1000, 100, 1),
                light = DisplayFactory.CreateLight(Color.Red, Status.On, 1000, 1000, 1),
                vibration = DisplayFactory.CreateVibration(Status.On, 1200, 100, 1)
            };

            using (Display display = DisplayFactory.InitializeDisplay(DisplayMode.BarcodeScan, 0, screen, _indicator))
            {
                return display;
            }
        }

        public static Display CreateException(string state, string header, string message, ButtonTypes button, string buttonUrl = null)
        {
            using (Display display = new Display())
            {
                display.mode = DisplayMode.BarcodeScan;
                display.sleep = 0;
                display.screen = new Screen();
                display.screen.header = header;

                var buzzer = DisplayFactory.CreateBuzzer(Status.On, 1000, 100, 1);
                var light = DisplayFactory.CreateLight(Color.Red, Status.On, 1000, 1000, 1);
                var vibration = DisplayFactory.CreateVibration(Status.On, 1200, 100, 1);

                display.indicator = new Indicator();
                display.indicator.buzzer = buzzer;
                display.indicator.vibration = vibration;
                display.indicator.light = light;
                display.screen.body = new List<List<ScreenElement>>();
                display.screen.state = state;

                var elemIcon = new List<ScreenElement>();
                elemIcon.Add(new ScreenElement
                {
                    type = "icon",
                    no = 217,
                    font_no = 3,
                    align = "center"
                });
                display.screen.body.Add(elemIcon);

                int chunkSize = 20;
                int stringLength = message.Length;
                for (int i = 0; i < stringLength; i += chunkSize)
                {
                    if (i + chunkSize > stringLength) chunkSize = stringLength - i;
                    {
                        var elemExceptionText = new List<ScreenElement>();
                        elemExceptionText.Add(new ScreenElement
                        {
                            type = "text",
                            value = message.Substring(i, chunkSize),
                            font_no = 2,
                            align = "center"
                        });
                        display.screen.body.Add(elemExceptionText);

                    }
                }

                // if state is eq loginscreen return main(device) controller
                if (state == "loginscreen")
                    buttonUrl = $"/{Constants.ServicePath}/device/devicecall";

                var elemExceptionButton = new List<ScreenElement>();
                elemExceptionButton.Add(new ScreenElement
                {
                    id = button.ToString(),
                    type = "button",
                    name = "OK",
                    font_no = 2,
                    align = "center",
                    url = buttonUrl
                });
                display.screen.body.Add(elemExceptionButton);
                return display;
            }
        }

        public Display GenerateDisplayByGivenScreen(Screen screen, OperationDataModel operationalData)
        {
            using (Display display = DisplayFactory.InitializeDisplay(DisplayMode.BarcodeScan, 0, screen, _indicator))
            {

                StateFactory<OperationDataModel>.SetOpetationData(_cacheUrl, _cacheKey, operationalData, DateTime.Now.AddMinutes(10));
                return display;
            }
        }

        public Display GenerateCaptureDisplayByGivenScreen(Screen screen, OperationDataModel operationalData)
        {
            using (Display display = DisplayFactory.InitializeDisplay(DisplayMode.Capture, 0, screen, _indicator))
            {

                StateFactory<OperationDataModel>.SetOpetationData(_cacheUrl, _cacheKey, operationalData, DateTime.Now.AddMinutes(10));
                return display;
            }
        }
    }
}
