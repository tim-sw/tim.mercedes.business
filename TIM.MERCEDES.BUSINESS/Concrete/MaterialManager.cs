﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using TIM.MERCEDES.BUSINESS.Abstract;
using TIM.MERCEDES.DATAACCESS.Abstract;
using TIM.MERCEDES.ENTITIES.Concrete;

namespace TIM.MERCEDES.BUSINESS.Concrete
{
    public class MaterialManager : ServiceBase<Material, IMaterialDAL>
    {
        public MaterialManager(IMaterialDAL _serviceDAL) : base(_serviceDAL)
        {
        }

        public override int Add(Material tEntity)
        {
            this.serviceDAL.Add(tEntity);
            return tEntity.Id;
        }

        public override void AddIfNotExist(Material entity, Expression<Func<Material, bool>> expression)
        {
            this.serviceDAL.AddIfNotExist(entity, expression);
        }

        public override void BulkInsert(List<Material> entityList)
        {
            this.serviceDAL.BulkInsert(entityList);
        }

        public override void BulkInsertOrUpdate(List<Material> entityList, string[] updateByProperties = null, string[] propertiesToExcludeOnUpdate = null)
        {
            this.serviceDAL.BulkInsertOrUpdate(entityList, updateByProperties, propertiesToExcludeOnUpdate);
        }

        public override void Delete(Material tEntity)
        {
            this.serviceDAL.Delete(tEntity);
        }

        public override Material Get(Expression<Func<Material, bool>> filter, bool includeRelations = false, params string[] relations)
        {
            return this.serviceDAL.Get(filter, includeRelations, relations);
        }

        public override List<Material> GetAll(Expression<Func<Material, bool>> filter = null, bool includeRelations = false, params string[] relations)
        {
            return this.serviceDAL.GetAll(filter, includeRelations, relations);
        }

        public override bool Update(Material tEntity)
        {
            try
            {
                this.serviceDAL.Update(tEntity);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }
}
