﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using TIM.MERCEDES.BUSINESS.Abstract;
using TIM.MERCEDES.DATAACCESS.Abstract;
using TIM.MERCEDES.ENTITIES.Concrete;

namespace TIM.MERCEDES.BUSINESS.Concrete
{
    public class CushionManager : ServiceBase<Cushion, ICushionDAL>
    {
        public CushionManager(ICushionDAL _serviceDAL) : base(_serviceDAL)
        {
        }

        public override int Add(Cushion tEntity)
        {
            this.serviceDAL.Add(tEntity);
            return tEntity.Id;
        }

        public override void AddIfNotExist(Cushion entity, Expression<Func<Cushion, bool>> expression)
        {
            this.serviceDAL.AddIfNotExist(entity, expression);
        }

        public override void BulkInsert(List<Cushion> entityList)
        {
            this.serviceDAL.BulkInsert(entityList);
        }

        public override void BulkInsertOrUpdate(List<Cushion> entityList, string[] updateByProperties = null, string[] propertiesToExcludeOnUpdate = null)
        {
            this.serviceDAL.BulkInsertOrUpdate(entityList, updateByProperties, propertiesToExcludeOnUpdate);
        }

        public override void Delete(Cushion tEntity)
        {
            this.serviceDAL.Delete(tEntity);
        }

        public override Cushion Get(Expression<Func<Cushion, bool>> filter, bool includeRelations = false, params string[] relations)
        {
            return this.serviceDAL.Get(filter, includeRelations, relations);
        }

        public override List<Cushion> GetAll(Expression<Func<Cushion, bool>> filter = null, bool includeRelations = false, params string[] relations)
        {
            return this.serviceDAL.GetAll(filter, includeRelations, relations);
        }

        public override bool Update(Cushion tEntity)
        {
            try
            {
                this.serviceDAL.Update(tEntity);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }
}
