﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using TIM.MERCEDES.BUSINESS.Abstract;
using TIM.MERCEDES.DATAACCESS.Abstract;
using TIM.MERCEDES.ENTITIES.Concrete;

namespace TIM.MERCEDES.BUSINESS.Concrete
{
    public class FaultyBBManager : ServiceBase<FaultyBB, IFaultyBBDAL>
    {
        public FaultyBBManager(IFaultyBBDAL _serviceDAL) : base(_serviceDAL)
        {
        }

        public override int Add(FaultyBB tEntity)
        {
            this.serviceDAL.Add(tEntity);
            return tEntity.Id;
        }

        public override void AddIfNotExist(FaultyBB entity, Expression<Func<FaultyBB, bool>> expression)
        {
            throw new NotImplementedException();
        }

        public override void BulkInsert(List<FaultyBB> entityList)
        {
            this.serviceDAL.BulkInsert(entityList);
        }

        public override void BulkInsertOrUpdate(List<FaultyBB> entityList, string[] updateByProperties = null, string[] propertiesToExcludeOnUpdate = null)
        {
            this.serviceDAL.BulkInsertOrUpdate(entityList, updateByProperties, propertiesToExcludeOnUpdate);
        }

        public override void Delete(FaultyBB tEntity)
        {
            this.serviceDAL.Delete(tEntity);
        }

        public override FaultyBB Get(Expression<Func<FaultyBB, bool>> filter, bool includeRelations = false, params string[] relations)
        {
            return this.serviceDAL.Get(filter, includeRelations, relations);
        }

        public override List<FaultyBB> GetAll(Expression<Func<FaultyBB, bool>> filter = null, bool includeRelations = false, params string[] relations)
        {
            return this.serviceDAL.GetAll(filter, includeRelations, relations);
        }

        public override bool Update(FaultyBB tEntity)
        {
            try
            {
                this.serviceDAL.Update(tEntity);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }
}
