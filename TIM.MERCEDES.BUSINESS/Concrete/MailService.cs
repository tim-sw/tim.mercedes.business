﻿using MailKit.Net.Smtp;
using MailKit.Security;
using Microsoft.Extensions.Options;
using MimeKit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using TIM.MERCEDES.BUSINESS.Abstract;
using TIM.MERCEDES.ENTITIES.Concrete.Models;
using TIM.MERCEDES.ENTITIES.Concrete.Settings;

namespace TIM.MERCEDES.BUSINESS.Concrete
{
    public class MailService : IMailService
    {
        private readonly MailSettings _mailConfigurations;
        public MailService(IOptions<MailSettings> mailConfigurations)
        {
            _mailConfigurations = mailConfigurations.Value;
        }
        public void SendEmail(MailRequest request)
        {
            try
            {
                //Generate mail message with configurations
                var emailMessage = new MimeMessage();
                emailMessage.From.Add(new MailboxAddress(_mailConfigurations.DisplayName, _mailConfigurations.FromEmail));


                if (request.ToEmails is null || !(request.ToEmails.Count > 0))
                    emailMessage.To.AddRange(_mailConfigurations.ToEmails?.Select(p => new MailboxAddress(p, p)));
                else
                    emailMessage.To.AddRange(request.ToEmails);

                if (request.CcEmails != null && request.CcEmails.Count > 0)
                    emailMessage.Cc.AddRange(request.CcEmails);

                else if(_mailConfigurations.CcEmails != null && _mailConfigurations.CcEmails.Count > 0)
                    emailMessage.Cc.AddRange(_mailConfigurations.CcEmails.Select(p => new MailboxAddress(p, p)));


                if (string.IsNullOrWhiteSpace(request.Subject))
                    emailMessage.Subject = _mailConfigurations.Subject;
                else
                    emailMessage.Subject = request.Subject;


                emailMessage.Body = new TextPart(MimeKit.Text.TextFormat.Html) { Text = request.Body };

                using (var client = new SmtpClient())
                {
                    try
                    {
                        client.Connect(_mailConfigurations.Host, _mailConfigurations.Port, SecureSocketOptions.None);
                        //client.AuthenticationMechanisms.Remove("XOAUTH2");
                        //client.Authenticate(_mailConfigurations.FromEmail, _mailConfigurations.Password);
                        //client.Authenticate(new NetworkCredential("ozmurat", "Mercedes12345", "EMEA"));

                        client.Send(emailMessage);
                    }
                    catch (Exception connectionException)
                    {
                        throw connectionException;
                    }
                    finally
                    {
                        client.Disconnect(true);
                        client.Dispose();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task SendEmailAsync(MailRequest request)
        {
            try
            {
                //Generate mail message with configurations
                var emailMessage = new MimeMessage();
                emailMessage.From.Add(new MailboxAddress("From", _mailConfigurations.FromEmail));



                if (!(request.ToEmails.Count > 0) || request.ToEmails is null)
                    emailMessage.To.AddRange(_mailConfigurations.ToEmails.Select(p => new MailboxAddress(p, p)));
                else
                    emailMessage.To.AddRange(request.ToEmails);

                if (!(request.CcEmails.Count > 0) || request.CcEmails is null)
                    emailMessage.To.AddRange(_mailConfigurations.CcEmails.Select(p => new MailboxAddress(p, p)));
                else
                    emailMessage.To.AddRange(request.CcEmails);

                if (string.IsNullOrWhiteSpace(request.Subject))
                    emailMessage.Subject = _mailConfigurations.Subject;
                else
                    emailMessage.Subject = request.Subject;

                emailMessage.Body = new TextPart(MimeKit.Text.TextFormat.Text) { Text = request.Body };

                using (var client = new SmtpClient())
                {
                    try
                    {
                        await client.ConnectAsync(_mailConfigurations.Host, _mailConfigurations.Port, SecureSocketOptions.StartTls);
                        //client.AuthenticationMechanisms.Remove("XOAUTH2");
                        await client.AuthenticateAsync(_mailConfigurations.FromEmail, _mailConfigurations.Password);

                        await client.SendAsync(emailMessage);
                    }
                    catch (Exception connectionException)
                    {
                        throw connectionException;
                    }
                    finally
                    {
                        await client.DisconnectAsync(true);
                        client.Dispose();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
