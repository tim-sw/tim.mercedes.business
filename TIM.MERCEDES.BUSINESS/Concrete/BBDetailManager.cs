﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using TIM.MERCEDES.BUSINESS.Abstract;
using TIM.MERCEDES.DATAACCESS.Abstract;
using TIM.MERCEDES.ENTITIES.Concrete;

namespace TIM.MERCEDES.BUSINESS.Concrete
{
    public class BBDetailManager : ServiceBase<BBDetail, IBBDetailDAL>
    {
        public BBDetailManager(IBBDetailDAL _serviceDAL) : base(_serviceDAL)
        {
        }

        public override int Add(BBDetail tEntity)
        {
            this.serviceDAL.Add(tEntity);
            return tEntity.Id;
        }

        public override void AddIfNotExist(BBDetail entity, Expression<Func<BBDetail, bool>> expression)
        {
            this.serviceDAL.AddIfNotExist(entity, expression);
        }

        public override void BulkInsert(List<BBDetail> entityList)
        {
            this.serviceDAL.BulkInsert(entityList);
        }

        public override void BulkInsertOrUpdate(List<BBDetail> entityList, string[] updateByProperties = null, string[] propertiesToExcludeOnUpdate = null)
        {
            this.serviceDAL.BulkInsertOrUpdate(entityList, updateByProperties, propertiesToExcludeOnUpdate);
        }

        public override void Delete(BBDetail tEntity)
        {
            this.serviceDAL.Delete(tEntity);
        }

        public override BBDetail Get(Expression<Func<BBDetail, bool>> filter, bool includeRelations = false, params string[] relations)
        {
            return this.serviceDAL.Get(filter, includeRelations, relations);
        }

        public override List<BBDetail> GetAll(Expression<Func<BBDetail, bool>> filter = null, bool includeRelations = false, params string[] relations)
        {
            return this.serviceDAL.GetAll(filter, includeRelations, relations);
        }

        public override bool Update(BBDetail tEntity)
        {
            try
            {
                this.serviceDAL.Update(tEntity);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }
}
