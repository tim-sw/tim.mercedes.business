﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using TIM.MERCEDES.BUSINESS.Abstract;
using TIM.MERCEDES.COMMON.Helpers;
using TIM.MERCEDES.DATAACCESS.Abstract;
using TIM.MERCEDES.ENTITIES.Concrete;
using TIM.MERCEDES.ENTITIES.Concrete.Models;
using TIM.MERCEDES.LOGGER.Abstract;

namespace TIM.MERCEDES.BUSINESS.Concrete
{
    public class BBDataManager : ServiceBase<BBData, IBBDataDAL>
    {
        private BBDetailManager bBDetailService;
        private SeatManager seatService;
        private MaterialManager materialService;
        private CushionManager cushionService;
        private string seat2GatherServiceUrl;
        private ILoggerManager loggerManager;

        public BBDataManager(IBBDataDAL _serviceDAL) : base(_serviceDAL)
        {
        }

        public BBDataManager(IBBDataDAL _serviceDAL, ISeatDAL seatDAL, IMaterialDAL materialDAL, ICushionDAL cushionDAL, IBBDetailDAL bBDetailDAL, string _seat2GatherServiceUrl) : base(_serviceDAL)
        {
            seatService = new SeatManager(seatDAL);
            materialService = new MaterialManager(materialDAL);
            cushionService = new CushionManager(cushionDAL);
            bBDetailService = new BBDetailManager(bBDetailDAL);
            seat2GatherServiceUrl = _seat2GatherServiceUrl;
        }

        public BBDataManager(IBBDataDAL _serviceDAL, ISeatDAL seatDAL, IMaterialDAL materialDAL, ICushionDAL cushionDAL, IBBDetailDAL bBDetailDAL, string _seat2GatherServiceUrl, ILoggerManager _loggerManager) : base(_serviceDAL)
        {
            seatService = new SeatManager(seatDAL);
            materialService = new MaterialManager(materialDAL);
            cushionService = new CushionManager(cushionDAL);
            bBDetailService = new BBDetailManager(bBDetailDAL);
            seat2GatherServiceUrl = _seat2GatherServiceUrl;
            loggerManager = _loggerManager;
        }

        public override int Add(BBData tEntity)
        {
            this.serviceDAL.Add(tEntity);
            return tEntity.Id;
        }

        public override void AddIfNotExist(BBData entity, Expression<Func<BBData, bool>> expression)
        {
            this.serviceDAL.AddIfNotExist(entity, expression);
        }

        public override void BulkInsertOrUpdate(List<BBData> entityList, string[] updateByProperties = null, string[] propertiesToExcludeOnUpdate = null)
        {
            this.serviceDAL.BulkInsertOrUpdate(entityList, updateByProperties, propertiesToExcludeOnUpdate);
        }

        public override void Delete(BBData tEntity)
        {
            this.serviceDAL.Delete(tEntity);
        }

        public override BBData Get(Expression<Func<BBData, bool>> filter, bool includeRelations = false, params string[] relations)
        {
            return this.serviceDAL.Get(filter, includeRelations, relations);
        }

        public override List<BBData> GetAll(Expression<Func<BBData, bool>> filter = null, bool includeRelations = false, params string[] relations)
        {
            return this.serviceDAL.GetAll(filter, includeRelations, relations);
        }

        public override bool Update(BBData tEntity)
        {
            try
            {
                this.serviceDAL.Update(tEntity);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public GetBBDataFromS2GatherResponse GetBBDataFromS2Gather(GetBBDataFromS2GatherRequest request)
        {
            try
            {
                ServiceHelper serviceHelper = new ServiceHelper(this.seat2GatherServiceUrl, loggerManager);
                var response = serviceHelper.GetBBData(request.startDate, request.endDate);
                if (response.ResponseResult.Result)
                {
                    var data = response.BBData.GroupBy(p => new { p.BBNumber }).Select(p => p.First()).ToList();
                    loggerManager.LogInformation(new StateLog(string.Empty, outputParameters: JsonConvert.SerializeObject(data), message: "Grouped BB Data", methodName: "GetBBDataFromS2Gather"));
                    this.BulkInsertOrUpdate(data, new string[] { "BBNumber" }, new string[] { "CreatedOn", "CreatedBy", "Status" });

                    List<Task> taskList = new List<Task>();

                    data.ForEach(item =>
                    {
                        var detailResponse = serviceHelper.GetBBNumberDetails(item.BBNumber);

                        if (detailResponse.ResponseResult.Result)
                        {
                            var seatData = seatService.GetAll(p => p.BBDataId == item.Id);

                            if (seatData.Count == 0)
                            {
                                var bbDetailTask = new Task(() =>
                                {
                                    detailResponse.Item1.ForEach(i => i.BBDataId = item.Id);
                                    bBDetailService.BulkInsert(detailResponse.Item1);
                                });

                                taskList.Add(bbDetailTask);
                                bbDetailTask.Start();

                                var materialTask = new Task(() =>
                                {
                                    detailResponse.Item2.ForEach(i => i.BBDataId = item.Id);
                                    materialService.BulkInsert(detailResponse.Item2);
                                });

                                taskList.Add(materialTask);
                                materialTask.Start();

                                var cushionTask = new Task(() =>
                                {
                                    detailResponse.Item3.ForEach(i => i.BBDataId = item.Id);
                                    cushionService.BulkInsert(detailResponse.Item3);
                                });

                                taskList.Add(cushionTask);
                                cushionTask.Start();

                                var seatTask = new Task(() =>
                                {
                                    detailResponse.Item4.ForEach(i => i.BBDataId = item.Id);
                                    seatService.BulkInsert(detailResponse.Item4);
                                });
                                taskList.Add(seatTask);
                                seatTask.Start();


                                Task.WaitAll(taskList.ToArray());
                                taskList.Clear();
                            }
                        }
                    });
                    response.BBData = data;
                }
                return response;
            }
            catch (Exception ex)
            {
                loggerManager.LogInformation(new StateLog(string.Empty, message: ex.ToString(), methodName: "GetBBDataFromS2Gather"));

                return new GetBBDataFromS2GatherResponse
                {
                    ResponseResult = ResponseResult.ReturnError(ex.Message)
                };
            }
        }

        public override void BulkInsert(List<BBData> entityList)
        {
            this.serviceDAL.BulkInsert(entityList);
        }
    }
}
