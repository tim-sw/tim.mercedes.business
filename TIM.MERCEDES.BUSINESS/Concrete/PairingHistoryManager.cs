﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using TIM.MERCEDES.BUSINESS.Abstract;
using TIM.MERCEDES.DATAACCESS.Abstract;
using TIM.MERCEDES.ENTITIES.Concrete;

namespace TIM.MERCEDES.BUSINESS.Concrete
{
    public class PairingHistoryManager : ServiceBase<PairingHistory, IPairingHistoryDAL>
    {
        public PairingHistoryManager(IPairingHistoryDAL _serviceDAL) : base(_serviceDAL)
        {
        }

        public override int Add(PairingHistory tEntity)
        {
            this.serviceDAL.Add(tEntity);
            return tEntity.Id;
        }

        public override void AddIfNotExist(PairingHistory entity, Expression<Func<PairingHistory, bool>> expression)
        {
            this.serviceDAL.AddIfNotExist(entity, expression);
        }

        public override void BulkInsert(List<PairingHistory> entityList)
        {
            this.serviceDAL.BulkInsert(entityList);
        }

        public override void BulkInsertOrUpdate(List<PairingHistory> entityList, string[] updateByProperties = null, string[] propertiesToExcludeOnUpdate = null)
        {
            this.serviceDAL.BulkInsertOrUpdate(entityList, updateByProperties, propertiesToExcludeOnUpdate);
        }

        public override void Delete(PairingHistory tEntity)
        {
            this.serviceDAL.Delete(tEntity);
        }

        public override PairingHistory Get(Expression<Func<PairingHistory, bool>> filter, bool includeRelations = false, params string[] relations)
        {
            return this.serviceDAL.Get(filter, includeRelations, relations);
        }

        public override List<PairingHistory> GetAll(Expression<Func<PairingHistory, bool>> filter = null, bool includeRelations = false, params string[] relations)
        {
            return this.serviceDAL.GetAll(filter, includeRelations, relations);
        }

        public override bool Update(PairingHistory tEntity)
        {
            try
            {
                this.serviceDAL.Update(tEntity);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }
}
