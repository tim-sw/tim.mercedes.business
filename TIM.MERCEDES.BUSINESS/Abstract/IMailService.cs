﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TIM.MERCEDES.ENTITIES.Concrete.Models;

namespace TIM.MERCEDES.BUSINESS.Abstract
{
    public interface IMailService
    {
        void SendEmail(MailRequest request);
        Task SendEmailAsync(MailRequest request);
    }
}
