﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using TIM.MERCEDES.DATAACCESS.Abstract;

namespace TIM.MERCEDES.BUSINESS.Abstract
{
    public abstract class ServiceBase<TEntity, TDALBase> 
        where TEntity: class, new()
        where TDALBase: IDALBase
    {
        public TDALBase serviceDAL { get; set; }
        public ServiceBase(TDALBase _serviceDAL)
        {
            serviceDAL = _serviceDAL;
        }
        public abstract List<TEntity> GetAll(Expression<Func<TEntity, bool>> filter = null, bool includeRelations = false, params string[] relations);
        public abstract TEntity Get(Expression<Func<TEntity, bool>> filter, bool includeRelations = false, params string[] relations);
        public abstract int Add(TEntity tEntity);
        public abstract bool Update(TEntity tEntity);
        public abstract void Delete(TEntity tEntity);
        public abstract void AddIfNotExist(TEntity entity, Expression<Func<TEntity, bool>> expression);
        public abstract void BulkInsertOrUpdate(List<TEntity> entityList, string[] updateByProperties = null, string[] propertiesToExcludeOnUpdate = null);
        public abstract void BulkInsert(List<TEntity> entityList);

    }
}
