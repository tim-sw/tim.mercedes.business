﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace TIM.MERCEDES.DATAACCESS.Abstract
{
    public interface IEntityRepository<TEntity> where TEntity: class, new()
    {
        List<TEntity> GetAll(Expression<Func<TEntity, bool>> filter = null, bool includeRelations = false, params string[] relations);
        TEntity Get(Expression<Func<TEntity, bool>> filter, bool includeRelations = false, params string[] relations);
        void Add(TEntity tEntity);
        void Update(TEntity tEntity);
        void Delete(TEntity tEntity);
        void AddIfNotExist(TEntity entity, Expression<Func<TEntity, bool>> expression);
        void BulkInsertOrUpdate(List<TEntity> entityList, string[] updateByProperties = null, string[] propertiesToExcludeOnUpdate = null);
        void BulkInsert(List<TEntity> entityList);
    }
}
