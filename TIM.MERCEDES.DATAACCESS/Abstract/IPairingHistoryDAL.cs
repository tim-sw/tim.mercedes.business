﻿using TIM.MERCEDES.ENTITIES.Concrete;

namespace TIM.MERCEDES.DATAACCESS.Abstract
{
    public interface IPairingHistoryDAL : IEntityRepository<PairingHistory>, IDALBase
    {
    }
}
