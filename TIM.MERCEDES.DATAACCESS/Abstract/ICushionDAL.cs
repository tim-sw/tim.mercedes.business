﻿using System;
using System.Collections.Generic;
using System.Text;
using TIM.MERCEDES.ENTITIES.Concrete;

namespace TIM.MERCEDES.DATAACCESS.Abstract
{
    public interface ICushionDAL : IEntityRepository<Cushion>, IDALBase
    {
    }
}
