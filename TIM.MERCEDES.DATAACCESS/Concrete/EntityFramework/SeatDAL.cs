﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;
using TIM.MERCEDES.DATAACCESS.Abstract;
using TIM.MERCEDES.ENTITIES.Concrete;

namespace TIM.MERCEDES.DATAACCESS.Concrete.EntityFramework
{
    public class SeatDAL : EntityRepositoryBase<Seat, MERCEDESContext>, ISeatDAL
    {
        public SeatDAL(IHttpContextAccessor httpContextAccessor) : base(httpContextAccessor)
        {
        }
    }
}
