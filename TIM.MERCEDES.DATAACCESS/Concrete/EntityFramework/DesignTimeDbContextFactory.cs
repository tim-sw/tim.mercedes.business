﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using System;
using System.Collections.Generic;
using System.Text;

namespace TIM.MERCEDES.DATAACCESS.Concrete.EntityFramework
{
    class DesignTimeDbContextFactory : IDesignTimeDbContextFactory<MERCEDESContext>
    {
        public MERCEDESContext CreateDbContext(string[] args)
        {
            var builder = new DbContextOptionsBuilder<MERCEDESContext>();
            var connectionString = "Server=DESKTOP-QUU3MD9\\SQLEXPRESS;Database=MERCEDES;Trusted_Connection=True;";
            builder.UseSqlServer(connectionString);
            return new MERCEDESContext(builder.Options);
        }
    }
}
