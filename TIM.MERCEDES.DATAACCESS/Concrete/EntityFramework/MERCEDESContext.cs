﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.Extensions.Configuration;
using TIM.MERCEDES.DATAACCESS.Concrete.EntityFramework.Configurations;
using TIM.MERCEDES.ENTITIES.Concrete;

#nullable disable

namespace TIM.MERCEDES.DATAACCESS.Concrete.EntityFramework
{
    public partial class MERCEDESContext : DbContext
    {
        public MERCEDESContext()
        {
        }

        public MERCEDESContext(DbContextOptions<MERCEDESContext> options)
            : base(options)
        {
        }

        public virtual DbSet<FaultyBB> FaultyBB { get; set; }
        public virtual DbSet<Seat> Seat { get; set; }
        public virtual DbSet<Material> Material { get; set; }
        public virtual DbSet<Cushion> Cushion { get; set; }
        public virtual DbSet<BBData> BBData { get; set; }
        public virtual DbSet<BBDetail> BBDetail { get; set; }
        public virtual DbSet<PairingHistory> PairingHistory{ get; set; }
        public virtual DbSet<StateLog> StateLog { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                IConfigurationRoot configuration = new ConfigurationBuilder()
                                                   .SetBasePath(AppDomain.CurrentDomain.BaseDirectory)
                                                   .AddJsonFile("appsettings.json")
                                                   .Build();
                optionsBuilder.UseSqlServer(configuration.GetConnectionString("MercedesDB"));
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new BBDataEntityConfiguration());
            modelBuilder.ApplyConfiguration(new SeatEntityConfiguration());
            modelBuilder.ApplyConfiguration(new MaterialEntityConfiguration());
            modelBuilder.ApplyConfiguration(new CushionEntityConfiguration());
            modelBuilder.ApplyConfiguration(new BBDetailsEntityConfiguration());
            modelBuilder.ApplyConfiguration(new FaultyBBEntityConfiguration());
            modelBuilder.ApplyConfiguration(new PairingHistoryEntityConfiguration());
            modelBuilder.ApplyConfiguration(new StateLogEntityConfiguration());
        }
    }
}
