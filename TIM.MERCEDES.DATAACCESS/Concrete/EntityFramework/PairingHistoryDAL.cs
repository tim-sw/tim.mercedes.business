﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;
using TIM.MERCEDES.DATAACCESS.Abstract;
using TIM.MERCEDES.ENTITIES.Concrete;

namespace TIM.MERCEDES.DATAACCESS.Concrete.EntityFramework
{
    public class PairingHistoryDAL : EntityRepositoryBase<PairingHistory, MERCEDESContext>, IPairingHistoryDAL
    {
        public PairingHistoryDAL(IHttpContextAccessor httpContextAccessor) : base(httpContextAccessor)
        {
        }
    }
}
