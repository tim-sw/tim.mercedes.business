﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using TIM.MERCEDES.DATAACCESS.Abstract;
using EFCore.BulkExtensions;
using TIM.MERCEDES.ENTITIES.Abstract;
using Microsoft.AspNetCore.Http;

namespace TIM.MERCEDES.DATAACCESS.Concrete.EntityFramework
{
    public class EntityRepositoryBase<TEntity, TContext> : IEntityRepository<TEntity>
        where TEntity : class, IEntity, new()
        where TContext : DbContext, new()
    {
        private readonly IHttpContextAccessor _httpContextAccessor;
        public EntityRepositoryBase(IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
        }

        public void Add(TEntity tEntity)
        {
            using (TContext context = new TContext())
            {
                this.SetUserInfo(tEntity);
                var addedEntity = context.Entry(tEntity);
                addedEntity.State = EntityState.Added;
                context.SaveChanges();
            }
        }

        public void BulkInsertOrUpdate(List<TEntity> entityList, string[] updateByProperties, string[] propertiesToExcludeOnUpdate)
        {
            using (TContext context = new TContext())
            {
                this.SetUserInfo(entityList);
                var options = new BulkConfig { UpdateByProperties = updateByProperties?.ToList(),
                    SetOutputIdentity = true,
                    PreserveInsertOrder = false,
                    PropertiesToExcludeOnUpdate = propertiesToExcludeOnUpdate?.ToList()
                };
                context.BulkInsertOrUpdate(entityList, options);
                context.SaveChanges();
            }
        }

        public void BulkInsert(List<TEntity> entityList)
        {
            using (TContext context = new TContext())
            {
                this.SetUserInfo(entityList);
                context.BulkInsert(entityList);
                context.SaveChanges();
            }
        }

        public void Delete(TEntity tEntity)
        {
            using (TContext context = new TContext())
            {
                var deletedEntity = context.Entry(tEntity);
                deletedEntity.State = EntityState.Deleted;
                context.SaveChanges();
            }
        }

        public TEntity Get(Expression<Func<TEntity, bool>> filter, bool includeRelations = false, params string[] relations)
        {
            using (TContext context = new TContext())
            {
                var ctx = context.Set<TEntity>().AsTracking().AsQueryable();
                if (includeRelations)
                {
                    foreach (var item in relations)
                    {
                        ctx = ctx.Include(item);
                    }
                    return ctx.SingleOrDefault(filter);
                }

                return context.Set<TEntity>().SingleOrDefault(filter);
            }
        }

        public List<TEntity> GetAll(Expression<Func<TEntity, bool>> filter = null, bool includeRelations = false, params string[] relations)
        {
            using (TContext context = new TContext())
            {
                var ctx = context.Set<TEntity>().AsQueryable();
                if (includeRelations)
                {
                    foreach (var item in relations)
                    {
                        ctx = ctx.Include(item);
                    }
                    return filter == null ? ctx.ToList() : ctx.Where(filter).ToList();
                }

                return filter == null ? context.Set<TEntity>().ToList() :
                                        context.Set<TEntity>().Where(filter).ToList();
            }
        }

        public void Update(TEntity tEntity)
        {
            using (TContext context = new TContext())
            {
                this.SetUserInfoForUpdate(tEntity);
                var updatedEntity = context.Entry(tEntity);
                updatedEntity.State = EntityState.Modified;
                context.SaveChanges();
            }
        }

        public void AddIfNotExist(TEntity entity, Expression<Func<TEntity, bool>> expression)
        {
            using(TContext context = new TContext())
            {
                var ctx = context.Set<TEntity>();
                var isExist = expression != null ? ctx.Any(expression) : ctx.Any();
                if (!isExist)
                {
                    ctx.Add(entity);
                    context.SaveChanges();
                }
            }
        }
        private string GetUserNameFromRequestHeader()
        {
            _httpContextAccessor.HttpContext?.Request.Headers.TryGetValue("UserName", out var userName);

            return userName;
        }

        private void SetUserInfo(TEntity entity)
        {

            var userName = this.GetUserNameFromRequestHeader();

            entity.ModifiedOn = DateTime.Now;
            entity.ModifiedBy = userName;

            entity.CreatedBy = userName;
            entity.CreatedOn = DateTime.Now;
        }

        private void SetUserInfo(List<TEntity> entityList)
        {
            var userName = this.GetUserNameFromRequestHeader();
            entityList.ForEach(p => { p.ModifiedOn = DateTime.Now; p.CreatedOn = DateTime.Now; p.CreatedBy = userName; p.ModifiedBy = userName; });
        }

        private void SetUserInfoForUpdate(TEntity entity)
        {
            var userName = this.GetUserNameFromRequestHeader();
            entity.ModifiedOn = DateTime.Now;
            entity.ModifiedBy = userName;
        }
    }
}
