﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System.Collections.Generic;
using TIM.MERCEDES.ENTITIES.Concrete;

namespace TIM.MERCEDES.DATAACCESS.Concrete.EntityFramework.Configurations
{
    public class PairingHistoryEntityConfiguration : IEntityTypeConfiguration<PairingHistory>
    {
        public void Configure(EntityTypeBuilder<PairingHistory> builder)
        {
            builder.Property(f => f.Id).ValueGeneratedOnAdd();
            builder.Property(p => p.Status).HasDefaultValue(1);
        }
    }
}
