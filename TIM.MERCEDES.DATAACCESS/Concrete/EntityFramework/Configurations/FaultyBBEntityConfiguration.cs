﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;
using TIM.MERCEDES.ENTITIES.Concrete;

namespace TIM.MERCEDES.DATAACCESS.Concrete.EntityFramework.Configurations
{
    public class FaultyBBEntityConfiguration : IEntityTypeConfiguration<FaultyBB>
    {
        public void Configure(EntityTypeBuilder<FaultyBB> builder)
        {
            builder.Property(f => f.Id).ValueGeneratedOnAdd();
            builder.Property(p => p.Status).HasDefaultValue(2);
        }
    }
}
