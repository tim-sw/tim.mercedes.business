﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;
using TIM.MERCEDES.ENTITIES.Concrete;

namespace TIM.MERCEDES.DATAACCESS.Concrete.EntityFramework.Configurations
{
    public class CushionEntityConfiguration : IEntityTypeConfiguration<Cushion>
    {
        public void Configure(EntityTypeBuilder<Cushion> builder)
        {
            builder.Property(f => f.Id).ValueGeneratedOnAdd();
        }
    }
}
