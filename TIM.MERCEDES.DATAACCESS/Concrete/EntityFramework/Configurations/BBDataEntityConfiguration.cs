﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;
using TIM.MERCEDES.ENTITIES.Concrete;

namespace TIM.MERCEDES.DATAACCESS.Concrete.EntityFramework.Configurations
{
    public class BBDataEntityConfiguration : IEntityTypeConfiguration<BBData>
    {
        public void Configure(EntityTypeBuilder<BBData> builder)
        {
            builder.Property(f => f.Id).ValueGeneratedOnAdd();
            builder.Property(p => p.Status).HasDefaultValue(1);
        }
    }
}
