﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;
using TIM.MERCEDES.ENTITIES.Concrete;

namespace TIM.MERCEDES.DATAACCESS.Concrete.EntityFramework.Configurations
{
    public class StateLogEntityConfiguration : IEntityTypeConfiguration<StateLog>
    {
        public void Configure(EntityTypeBuilder<StateLog> builder)
        {
            builder.Property(f => f.Id).ValueGeneratedOnAdd();
            builder.Property(p => p.CreatedOn).HasDefaultValueSql("GETDATE()");
        }
    }
}
