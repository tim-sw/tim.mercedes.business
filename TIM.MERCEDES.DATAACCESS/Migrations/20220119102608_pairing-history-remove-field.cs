﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TIM.MERCEDES.DATAACCESS.Migrations
{
    public partial class pairinghistoryremovefield : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "UserName",
                table: "PairingHistory");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "UserName",
                table: "PairingHistory",
                type: "nvarchar(max)",
                nullable: true);
        }
    }
}
