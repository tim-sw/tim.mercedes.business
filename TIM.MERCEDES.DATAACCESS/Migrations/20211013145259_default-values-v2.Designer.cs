﻿// <auto-generated />
using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using TIM.MERCEDES.DATAACCESS.Concrete.EntityFramework;

namespace TIM.MERCEDES.DATAACCESS.Migrations
{
    [DbContext(typeof(MERCEDESContext))]
    [Migration("20211013145259_default-values-v2")]
    partial class defaultvaluesv2
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("Relational:MaxIdentifierLength", 128)
                .HasAnnotation("ProductVersion", "5.0.10")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("TIM.MERCEDES.ENTITIES.Concrete.BBData", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("BBNumber")
                        .HasColumnType("nvarchar(max)");

                    b.Property<DateTime>("CreatedOn")
                        .HasColumnType("datetime2");

                    b.Property<DateTime>("ModifiedOn")
                        .HasColumnType("datetime2");

                    b.HasKey("Id");

                    b.ToTable("BBData");
                });

            modelBuilder.Entity("TIM.MERCEDES.ENTITIES.Concrete.BBDetail", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<int>("BBDataId")
                        .HasColumnType("int");

                    b.Property<DateTime>("CreatedOn")
                        .HasColumnType("datetime2");

                    b.Property<string>("Explanation1")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("Explanation2")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("Explanation3")
                        .HasColumnType("nvarchar(max)");

                    b.Property<DateTime>("ModifiedOn")
                        .HasColumnType("datetime2");

                    b.Property<string>("SeatType")
                        .HasColumnType("nvarchar(max)");

                    b.Property<int>("SeatTypeId")
                        .HasColumnType("int");

                    b.HasKey("Id");

                    b.HasIndex("BBDataId");

                    b.ToTable("BBDetail");
                });

            modelBuilder.Entity("TIM.MERCEDES.ENTITIES.Concrete.Cushion", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<int>("BBDataId")
                        .HasColumnType("int");

                    b.Property<DateTime>("CreatedOn")
                        .HasColumnType("datetime2");

                    b.Property<string>("MaterialName")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("MaterialNumber")
                        .HasColumnType("nvarchar(max)");

                    b.Property<DateTime>("ModifiedOn")
                        .HasColumnType("datetime2");

                    b.Property<string>("OrderNumber")
                        .HasColumnType("nvarchar(max)");

                    b.Property<int>("Quantity")
                        .HasColumnType("int");

                    b.Property<string>("Unit")
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("Id");

                    b.HasIndex("BBDataId");

                    b.ToTable("Cushion");
                });

            modelBuilder.Entity("TIM.MERCEDES.ENTITIES.Concrete.FaultyBB", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<int>("BBDataId")
                        .HasColumnType("int");

                    b.Property<string>("BBNumber")
                        .HasColumnType("nvarchar(max)");

                    b.Property<DateTime>("CreatedOn")
                        .HasColumnType("datetime2");

                    b.Property<DateTime?>("Date")
                        .HasColumnType("datetime2");

                    b.Property<string>("Description")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("ImageBase64")
                        .HasColumnType("nvarchar(max)");

                    b.Property<DateTime>("ModifiedOn")
                        .HasColumnType("datetime2");

                    b.Property<int>("SeatId")
                        .HasColumnType("int");

                    b.Property<int>("Status")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasDefaultValue(2);

                    b.HasKey("Id");

                    b.HasIndex("BBDataId");

                    b.ToTable("FaultyBB");
                });

            modelBuilder.Entity("TIM.MERCEDES.ENTITIES.Concrete.Material", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<int>("BBDataId")
                        .HasColumnType("int");

                    b.Property<DateTime>("CreatedOn")
                        .HasColumnType("datetime2");

                    b.Property<string>("Description")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("Group")
                        .HasColumnType("nvarchar(max)");

                    b.Property<int>("GroupName")
                        .HasColumnType("int");

                    b.Property<string>("MaterialNumber")
                        .HasColumnType("nvarchar(max)");

                    b.Property<DateTime>("ModifiedOn")
                        .HasColumnType("datetime2");

                    b.Property<string>("Name")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("OrderNumber")
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("Id");

                    b.HasIndex("BBDataId");

                    b.ToTable("Material");
                });

            modelBuilder.Entity("TIM.MERCEDES.ENTITIES.Concrete.Seat", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<int>("BBDataId")
                        .HasColumnType("int");

                    b.Property<DateTime>("CreatedOn")
                        .HasColumnType("datetime2");

                    b.Property<DateTime>("ModifiedOn")
                        .HasColumnType("datetime2");

                    b.Property<string>("OrderNumber")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("SeatCode")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("SeatCodeType")
                        .HasColumnType("nvarchar(max)");

                    b.Property<int>("Status")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasDefaultValue(2);

                    b.HasKey("Id");

                    b.HasIndex("BBDataId");

                    b.ToTable("Seat");
                });

            modelBuilder.Entity("TIM.MERCEDES.ENTITIES.Concrete.BBDetail", b =>
                {
                    b.HasOne("TIM.MERCEDES.ENTITIES.Concrete.BBData", null)
                        .WithMany("BBDetails")
                        .HasForeignKey("BBDataId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });

            modelBuilder.Entity("TIM.MERCEDES.ENTITIES.Concrete.Cushion", b =>
                {
                    b.HasOne("TIM.MERCEDES.ENTITIES.Concrete.BBData", null)
                        .WithMany("Cushions")
                        .HasForeignKey("BBDataId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });

            modelBuilder.Entity("TIM.MERCEDES.ENTITIES.Concrete.FaultyBB", b =>
                {
                    b.HasOne("TIM.MERCEDES.ENTITIES.Concrete.BBData", null)
                        .WithMany("FaultyBBs")
                        .HasForeignKey("BBDataId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });

            modelBuilder.Entity("TIM.MERCEDES.ENTITIES.Concrete.Material", b =>
                {
                    b.HasOne("TIM.MERCEDES.ENTITIES.Concrete.BBData", null)
                        .WithMany("Materials")
                        .HasForeignKey("BBDataId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });

            modelBuilder.Entity("TIM.MERCEDES.ENTITIES.Concrete.Seat", b =>
                {
                    b.HasOne("TIM.MERCEDES.ENTITIES.Concrete.BBData", null)
                        .WithMany("Seats")
                        .HasForeignKey("BBDataId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });

            modelBuilder.Entity("TIM.MERCEDES.ENTITIES.Concrete.BBData", b =>
                {
                    b.Navigation("BBDetails");

                    b.Navigation("Cushions");

                    b.Navigation("FaultyBBs");

                    b.Navigation("Materials");

                    b.Navigation("Seats");
                });
#pragma warning restore 612, 618
        }
    }
}
