﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TIM.MERCEDES.DATAACCESS.Migrations
{
    public partial class fixdefaultvalues : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<int>(
                        name: "Status",
                        table: "BBData",
                        type: "int",
                        nullable: false,
                        defaultValue: 1);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
