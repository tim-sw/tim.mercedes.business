﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace TIM.MERCEDES.DATAACCESS.Migrations
{
    public partial class init : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "BBData",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    BBNumber = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BBData", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "BBDetail",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    BBDataId = table.Column<int>(type: "int", nullable: false),
                    FaultyBBId = table.Column<int>(type: "int", nullable: false),
                    Explanation1 = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Explanation2 = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Explanation3 = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    SeatTypeId = table.Column<int>(type: "int", nullable: false),
                    SeatType = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BBDetail", x => x.Id);
                    table.ForeignKey(
                        name: "FK_BBDetail_BBData_BBDataId",
                        column: x => x.BBDataId,
                        principalTable: "BBData",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Cushion",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    BBDataId = table.Column<int>(type: "int", nullable: false),
                    OrderNumber = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    MaterialNumber = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    MaterialName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Quantity = table.Column<int>(type: "int", nullable: false),
                    Unit = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Cushion", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Cushion_BBData_BBDataId",
                        column: x => x.BBDataId,
                        principalTable: "BBData",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "FaultyBB",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    BBDataId = table.Column<int>(type: "int", nullable: false),
                    BBNumber = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    SeatId = table.Column<int>(type: "int", nullable: false),
                    Description = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Date = table.Column<DateTime>(type: "datetime2", nullable: true),
                    ImageBase64 = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Status = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FaultyBB", x => x.Id);
                    table.ForeignKey(
                        name: "FK_FaultyBB_BBData_BBDataId",
                        column: x => x.BBDataId,
                        principalTable: "BBData",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Material",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    BBDataId = table.Column<int>(type: "int", nullable: false),
                    OrderNumber = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    MaterialNumber = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Description = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    GroupName = table.Column<int>(type: "int", nullable: false),
                    Group = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Material", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Material_BBData_BBDataId",
                        column: x => x.BBDataId,
                        principalTable: "BBData",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Seat",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    BBDataId = table.Column<int>(type: "int", nullable: false),
                    FaultyBBId = table.Column<int>(type: "int", nullable: false),
                    OrderNumber = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    SeatCodeType = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    SeatCode = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Status = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Seat", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Seat_BBData_BBDataId",
                        column: x => x.BBDataId,
                        principalTable: "BBData",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_BBDetail_BBDataId",
                table: "BBDetail",
                column: "BBDataId");

            migrationBuilder.CreateIndex(
                name: "IX_Cushion_BBDataId",
                table: "Cushion",
                column: "BBDataId");

            migrationBuilder.CreateIndex(
                name: "IX_FaultyBB_BBDataId",
                table: "FaultyBB",
                column: "BBDataId");

            migrationBuilder.CreateIndex(
                name: "IX_Material_BBDataId",
                table: "Material",
                column: "BBDataId");

            migrationBuilder.CreateIndex(
                name: "IX_Seat_BBDataId",
                table: "Seat",
                column: "BBDataId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "BBDetail");

            migrationBuilder.DropTable(
                name: "Cushion");

            migrationBuilder.DropTable(
                name: "FaultyBB");

            migrationBuilder.DropTable(
                name: "Material");

            migrationBuilder.DropTable(
                name: "Seat");

            migrationBuilder.DropTable(
                name: "BBData");
        }
    }
}
