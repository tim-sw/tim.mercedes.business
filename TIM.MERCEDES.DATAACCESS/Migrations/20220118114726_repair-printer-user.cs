﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace TIM.MERCEDES.DATAACCESS.Migrations
{
    public partial class repairprinteruser : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "PrintDate",
                table: "Seat",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "PrintUser",
                table: "Seat",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "RepairedBy",
                table: "FaultyBB",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "RepairedDate",
                table: "FaultyBB",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "PrintDate",
                table: "Seat");

            migrationBuilder.DropColumn(
                name: "PrintUser",
                table: "Seat");

            migrationBuilder.DropColumn(
                name: "RepairedBy",
                table: "FaultyBB");

            migrationBuilder.DropColumn(
                name: "RepairedDate",
                table: "FaultyBB");
        }
    }
}
