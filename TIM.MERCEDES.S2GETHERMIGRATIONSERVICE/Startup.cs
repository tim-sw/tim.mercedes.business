using Hangfire;
using Hangfire.SqlServer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TIM.MERCEDES.DATAACCESS.Abstract;
using TIM.MERCEDES.DATAACCESS.Concrete.EntityFramework;
using TIM.MERCEDES.LOGGER.Abstract;
using TIM.MERCEDES.LOGGER.Concrete.Log4Net;

namespace TIM.MERCEDES.S2GETHERMIGRATIONSERVICE
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddHangfire(config =>
            {
                var option = new SqlServerStorageOptions
                {
                    PrepareSchemaIfNecessary = true,
                    QueuePollInterval = TimeSpan.FromSeconds(5),
                    CommandBatchMaxTimeout = TimeSpan.FromSeconds(5),
                    SlidingInvisibilityTimeout = TimeSpan.FromSeconds(5),
                    UseRecommendedIsolationLevel = true,
                    UsePageLocksOnDequeue = true,
                    DisableGlobalLocks = true,
                };

                config.UseSqlServerStorage(Configuration.GetConnectionString("MercedesDB"), option).WithJobExpirationTimeout(TimeSpan.FromHours(6));
            });

            services.AddHangfireServer();

            services.AddHttpContextAccessor();
            services.AddTransient<IBBDataDAL, BBDataDAL>();
            services.AddTransient<ISeatDAL, SeatDAL>();
            services.AddTransient<IFaultyBBDAL, FaultyBBDAL>();
            services.AddTransient<IMaterialDAL, MaterialDAL>();
            services.AddTransient<ICushionDAL, CushionDAL>();
            services.AddTransient<IBBDetailDAL, BBDetailDAL>();
            services.AddTransient<IPairingHistoryDAL, PairingHistoryDAL>();
            services.AddTransient<ILoggerManager, Log4NetManager>();

            services.AddControllers();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
                endpoints.MapHangfireDashboard();
            });
        }
    }
}
