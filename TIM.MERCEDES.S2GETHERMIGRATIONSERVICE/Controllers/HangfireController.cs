﻿using Hangfire;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using TIM.MERCEDES.BUSINESS.Concrete;
using TIM.MERCEDES.DATAACCESS.Abstract;
using TIM.MERCEDES.ENTITIES.Concrete;
using TIM.MERCEDES.ENTITIES.Concrete.Models;
using TIM.MERCEDES.LOGGER.Abstract;

namespace TIM.MERCEDES.S2GETHERMIGRATIONSERVICE.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class HangfireController : ControllerBase
    {
        private BBDataManager bBDataService;
        private IConfiguration configuration;
        private ILoggerManager loggerManager;
        private IHttpContextAccessor httpContextAccessor;
        public string seat2GatherServiceUrl;

        public HangfireController(IConfiguration _configuration, IBBDataDAL bBNumberDAL, ISeatDAL seatDAL, IMaterialDAL materialDAL, ICushionDAL cushionDAL, IBBDetailDAL bBDetailDAL, IHttpContextAccessor _httpContextAccessor, ILoggerManager _loggerManager)
        {
            configuration = _configuration;
            seat2GatherServiceUrl = configuration["S2GatherServiceUrl"];
            loggerManager = _loggerManager;
            httpContextAccessor = _httpContextAccessor;
            bBDataService = new BBDataManager(bBNumberDAL, seatDAL, materialDAL, cushionDAL, bBDetailDAL, seat2GatherServiceUrl, loggerManager);
        }

        /// <summary>
        /// This method adding or updating recurring jobs for hangfire
        /// Recurring jobs run daily and time information read from appsettings
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("AddOrUpdateS2GetherJob")]
        public IActionResult AddOrUpdateS2GetherJob()
        {
            var shceduleJobHour = configuration["ScheduleJobHour"];
            var shceduleJobMinute = configuration["ScheduleJobMinute"];
            // hangfire is working with utc timezone, minus 3 is required for turkey timezone
            RecurringJob.AddOrUpdate(() => MigrationProcess(), Cron.Daily(Convert.ToInt32(shceduleJobHour) - 3, Convert.ToInt32(shceduleJobMinute)));
            return Ok($"Recurring Job Scheduled.");
        }

        /// <summary>
        /// This method for S2Gether migration 
        /// </summary>
        public void MigrationProcess()
        {
            try
            {

                var request = new GetBBDataFromS2GatherRequest
                {
                    startDate = DateTime.Now.AddMonths(-6),
                    endDate = DateTime.Now.AddMonths(6)
                };
                loggerManager.LogInformation(new StateLog(userName: "Migration", methodName: nameof(MigrationProcess), message: $"Migration process started {DateTime.Now}", inputParameters: JsonConvert.SerializeObject(request)));
                httpContextAccessor.HttpContext = new DefaultHttpContext();
                httpContextAccessor.HttpContext.Request.Headers.Add("UserName", "Migration");

                var response = bBDataService.GetBBDataFromS2Gather(request);

                loggerManager.LogInformation(new StateLog(userName: "Migration", methodName: nameof(MigrationProcess), message: $"Migration process done {DateTime.Now}", inputParameters: JsonConvert.SerializeObject(response)));

            }
            catch (Exception ex)
            {
                loggerManager.LogInformation(new StateLog(userName: "Migration", methodName: nameof(MigrationProcess), message: $"Exception : {ex.ToString()}"));
                throw ex;
            }
        }
    }
}
