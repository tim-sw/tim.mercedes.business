﻿using System;
using System.Collections.Generic;
using System.Text;
using TIM.MERCEDES.ENTITIES.Concrete;

namespace TIM.MERCEDES.LOGGER.Abstract
{
    public interface ILoggerManager
    {
        void LogInformation(string message);
        void LogDebug(string message);
        void LogInformation(StateLog logs);
        void LogError(string message);
        void LogWarning(string message);
    }
}
