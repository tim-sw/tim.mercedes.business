﻿using log4net;
using log4net.Config;
using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text;
using System.Xml;
using TIM.MERCEDES.ENTITIES.Concrete;
using TIM.MERCEDES.LOGGER.Abstract;

namespace TIM.MERCEDES.LOGGER.Concrete.Log4Net
{
    public class Log4NetManager : ILoggerManager
    {
        private readonly ILog _infoFileLogger = LogManager.GetLogger("InfoFileLogger");
        private readonly ILog _infoAdoNetLogger = LogManager.GetLogger("InfoAdoNETLogger");
        private readonly ILog _consoleLogger = LogManager.GetLogger("ConsoleLogger");

        public Log4NetManager(string logName = "CustomLog")
        {
            try
            {
                XmlDocument log4netConfig = new XmlDocument();

                using (var fs = File.OpenRead("log4net.config"))
                {
                    log4netConfig.Load(fs);

                    var repo = LogManager.CreateRepository(Assembly.GetEntryAssembly(), typeof(log4net.Repository.Hierarchy.Hierarchy));

                    log4net.GlobalContext.Properties["LogName"] = logName;

                    XmlConfigurator.Configure(repo, log4netConfig["log4net"]);
                }
            }
            catch (Exception)
            {

                throw;
            }
        }
        public void LogInformation(string message)
        {
            _infoFileLogger.Info(message);
        }

        public void LogInformation(StateLog stateLog)
        {
            log4net.LogicalThreadContext.Properties["UserName"] = stateLog.UserName;
            log4net.LogicalThreadContext.Properties["DeviceId"] = stateLog.DeviceId;
            log4net.LogicalThreadContext.Properties["InputParameters"] = stateLog.InputParameters;
            log4net.LogicalThreadContext.Properties["OutputParameters"] = stateLog.OutputParameters;
            log4net.LogicalThreadContext.Properties["MethodName"] = stateLog.MethodName;
            log4net.LogicalThreadContext.Properties["Message"] = stateLog.Message;
            _infoAdoNetLogger.Info(stateLog.Message);
        }

        public void LogError(string message)
        {
            _infoFileLogger.Error(message);
        }

        public void LogWarning(string message)
        {
            _infoFileLogger.Warn(message);
        }

        public void LogDebug(string message)
        {
            _consoleLogger.Debug(message);
        }
    }
}
