﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using System;
using TIM.MERCEDES.BUSINESS.Concrete;
using TIM.MERCEDES.DATAACCESS.Abstract;
using TIM.MERCEDES.ENTITIES.Concrete.Models;
using TIM.MERCEDES.LOGGER.Abstract;
using TIM.MERCEDES.S2GETHERMIGRATIONAPP.Helpers;

namespace TIM.MERCEDES.S2GETHERMIGRATIONAPP
{
    internal class Program
    {
        static void Main(string[] args)
        {
            var configuration = ConfigurationHelper.BuildConfiguration();

            var serviceProvider = ServiceHelper.BuildServices();
            var loggerManager = serviceProvider.GetService<ILoggerManager>();

            loggerManager.LogDebug($"Migration App Start");

            var request = new GetBBDataFromS2GatherRequest
            {
                startDate = DateTime.Now.AddMonths(-6),
                endDate = DateTime.Now.AddMonths(6)
            };

            loggerManager.LogDebug($"Migration request {JsonConvert.SerializeObject(request)}");

            var bBNumberDAL = serviceProvider.GetService<IBBDataDAL>();
            var seatDAL = serviceProvider.GetService<ISeatDAL>();
            var materialDAL = serviceProvider.GetService<IMaterialDAL>();
            var cushionDAL = serviceProvider.GetService<ICushionDAL>();
            var bBDetailDAL = serviceProvider.GetService<IBBDetailDAL>();

            var httpContextAccessor = serviceProvider.GetService<IHttpContextAccessor>();
            httpContextAccessor.HttpContext = new DefaultHttpContext();
            httpContextAccessor.HttpContext.Request.Headers.Add("UserName", "Migration");

            loggerManager.LogDebug($"Migration service initialization done.");

            var seat2GatherServiceUrl = configuration["S2GatherServiceUrl"];

            loggerManager.LogDebug($"Migration Service Url : {seat2GatherServiceUrl}");

            var bBDataService = new BBDataManager(bBNumberDAL, seatDAL, materialDAL, cushionDAL, bBDetailDAL, seat2GatherServiceUrl, loggerManager);

            loggerManager.LogDebug($"BBDataManager initialization done.");

            loggerManager.LogDebug($"Migration Started {DateTime.Now}");
            var response = bBDataService.GetBBDataFromS2Gather(request);
            loggerManager.LogDebug($"Migration Done {DateTime.Now}");
        }
    }
}
