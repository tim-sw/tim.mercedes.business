﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;

namespace TIM.MERCEDES.S2GETHERMIGRATIONAPP.Helpers
{
    public class ConfigurationHelper
    {
        public ConfigurationHelper()
        {
        }
        public static IConfiguration BuildConfiguration()
        {
            var builder = new ConfigurationBuilder().AddJsonFile("appsettings.json", true, true);
            var configuration = builder.Build();
            return configuration;
        }
    }
}
