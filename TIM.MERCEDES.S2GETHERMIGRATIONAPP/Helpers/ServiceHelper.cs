﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;
using TIM.MERCEDES.DATAACCESS.Abstract;
using TIM.MERCEDES.DATAACCESS.Concrete.EntityFramework;
using TIM.MERCEDES.LOGGER.Abstract;
using TIM.MERCEDES.LOGGER.Concrete.Log4Net;

namespace TIM.MERCEDES.S2GETHERMIGRATIONAPP.Helpers
{
    public class ServiceHelper
    {
        public static ServiceProvider BuildServices()
        {
            ServiceProvider serviceProvider = new ServiceCollection()
                .AddTransient<IBBDataDAL, BBDataDAL>()
                .AddTransient<ISeatDAL, SeatDAL>()
                .AddTransient<IFaultyBBDAL, FaultyBBDAL>()
                .AddTransient<IMaterialDAL, MaterialDAL>()
                .AddTransient<ICushionDAL, CushionDAL>()
                .AddTransient<IBBDetailDAL, BBDetailDAL>()
                .AddTransient<IPairingHistoryDAL, PairingHistoryDAL>()

                .AddTransient<ILoggerManager, Log4NetManager>()

                .AddSingleton<IHttpContextAccessor, HttpContextAccessor>()
                .BuildServiceProvider();
            return serviceProvider;
        }
    }
}
