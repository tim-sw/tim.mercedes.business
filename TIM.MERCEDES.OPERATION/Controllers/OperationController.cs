﻿using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIM.MERCEDES.BUSINESS.Abstract;
using TIM.MERCEDES.BUSINESS.Concrete;
using TIM.MERCEDES.COMMON.Enums;
using TIM.MERCEDES.COMMON.Helpers;
using TIM.MERCEDES.DATAACCESS.Abstract;
using TIM.MERCEDES.ENTITIES.Concrete;
using TIM.MERCEDES.ENTITIES.Concrete.Models;
using TIM.MERCEDES.LOGGER.Abstract;

namespace TIM.MERCEDES.OPERATION.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class OperationController : ControllerBase
    {
        private BBDataManager bBDataService;
        private BBDetailManager bBDetailService;
        private SeatManager seatService;
        private FaultyBBManager faultyBBService;
        private MaterialManager materialService;
        private CushionManager cushionService;
        private PairingHistoryManager pairingHistoryService;
        private IMailService _mailService;
        private IConfiguration configuration;
        private ILoggerManager loggerManager;
        public string seat2GatherServiceUrl;

        public OperationController(IConfiguration _configuration, IBBDataDAL bBNumberDAL, ISeatDAL seatDAL, IFaultyBBDAL faultyBBDAL, IMaterialDAL materialDAL, ICushionDAL cushionDAL, IBBDetailDAL bBDetailDAL, IPairingHistoryDAL pairingHistoryDAL, ILoggerManager _loggerManager, IMailService mailService)
        {
            configuration = _configuration;
            seat2GatherServiceUrl = configuration["S2GatherServiceUrl"];
            loggerManager = _loggerManager;

            bBDataService = new BBDataManager(bBNumberDAL, seatDAL, materialDAL, cushionDAL, bBDetailDAL, seat2GatherServiceUrl, loggerManager);
            seatService = new SeatManager(seatDAL);
            faultyBBService = new FaultyBBManager(faultyBBDAL);
            materialService = new MaterialManager(materialDAL);
            cushionService = new CushionManager(cushionDAL);
            bBDetailService = new BBDetailManager(bBDetailDAL);
            pairingHistoryService = new PairingHistoryManager(pairingHistoryDAL);
            _mailService = mailService;
        }

        private string GetUserName()
        {
            Request.Headers.TryGetValue("UserName", out var userName);
            return userName;
        }

        [HttpGet]
        [Route("testme")]
        public string testme()
        {
            //RedisHelper redisHelper = new RedisHelper(configuration["CacheUrl"]);

            //var data = new CacheInformation
            //{
            //    IsDataRetrived = true,
            //    RetrivedDate = DateTime.Now,
            //};

            //redisHelper.SaveCache(Constants.CacheKeyForWeeklyData, JsonConvert.SerializeObject(data), DateTime.Now.AddMinutes(2));

            //loggerManager.LogInformation(new StateLog(GetUserName(), inputParameters: "", outputParameters: "", methodName: "testme", message: $"Test"));

            //loggerManager.LogInformation("i am ok");
            //try
            //{
            //    var emailBody = new StringBuilder();
            //    emailBody.AppendLine("<p>Merhaba,</p>");
            //    emailBody.AppendLine($"<p>{DateTime.Now.ToString("dd/MM/yyyy")} tarihinde 123 no.lu araç ile 123 no.lu araç {GetUserName()} kullanıcısı tarafından eşleştirilmiştir.</p>");
            //    emailBody.AppendLine("<p>Kontrolünün sağlanması ve gerekli değişikliklerin yapılması adına bilgilerinize.</p>");
            //    emailBody.AppendLine("<p>İlgili rapora aşağıda yer alan link üzerinden ulaşabilirsiniz.</p>");
            //    emailBody.AppendLine($"<p><a href={configuration["UIUrl"]}>Rapor Link</a></p>");

            //    _mailService.SendEmail(new MailRequest(emailBody.ToString()));
            //}
            //catch (Exception ex)
            //{
            //    return ex.Message;
            //}

            //var data = new BBNumber
            //{
            //    Number = "BB6280240455"
            //};
            //bBDataervice.AddIfNotExist(data, x => x.Number == data.Number);
            //var data = bBDataervice.GetAll(null, true, "Seats", "Materials", "BBDetails");
            //var data1 = seatService.Get(f => f.BBNumberId == data[0].Id);
            //System.Threading.Thread.Sleep(5000);
            return "i am ok";
        }

        [HttpPost]
        [Route("Login")]
        public LoginResponse Login([FromBody] LoginRequest request)
        {
            try
            {
                loggerManager.LogInformation(new StateLog(request.userName, methodName: "Login", inputParameters: JsonConvert.SerializeObject(request)));
                var ldapDomain = configuration["LDAP_DOMAIN"];
                var ldapPort = Convert.ToInt32(configuration["LDAP_PORT"]);
                loggerManager.LogInformation(new StateLog(request.userName, methodName: "Login", message: ($"LDAP_DOMAIN: {ldapDomain} , LDAP_PORT: {ldapPort}")));

                AuthenticationHelper authenticationHelper = new AuthenticationHelper(ldapDomain, ldapPort, loggerManager);
                var response = authenticationHelper.AuthenticateWithLDAP(request.userName, request.password);
                loggerManager.LogInformation(new StateLog(request.userName, methodName: "Login", outputParameters: response.ToString()));

                if (!response)
                    return new LoginResponse
                    {
                        isAuthenticationValid = false,
                        ResponseResult = ResponseResult.ReturnError("Hatalı kullanıcı")
                    };


                return new LoginResponse
                {
                    isAuthenticationValid = true,
                    ResponseResult = ResponseResult.ReturnSuccess()
                };
            }
            catch (Exception ex)
            {
                return new LoginResponse
                {
                    ResponseResult = ResponseResult.ReturnError(ex.Message)
                };
            }
        }

        [HttpPost]
        [Route("GetBBDataFromS2Gather")]
        public GetBBDataFromS2GatherResponse GetBBDataFromS2Gather([FromBody] GetBBDataFromS2GatherRequest request)
        {
            try
            {
                loggerManager.LogInformation(new StateLog(GetUserName(), inputParameters: JsonConvert.SerializeObject(request), methodName: nameof(GetBBDataFromS2Gather)));
                var response = bBDataService.GetBBDataFromS2Gather(request);
                loggerManager.LogInformation(new StateLog(GetUserName(), outputParameters: JsonConvert.SerializeObject(response), methodName: nameof(GetBBDataFromS2Gather)));
                return response;
            }
            catch (Exception ex)
            {
                loggerManager.LogInformation(new StateLog(GetUserName(), methodName: nameof(GetBBDataFromS2Gather), message: ex.ToString()));
                return new GetBBDataFromS2GatherResponse
                {
                    ResponseResult = ResponseResult.ReturnError(ex.Message)
                };
            }
        }

        [HttpPost]
        [Route("GetAllBBData")]
        public GetAllBBDataResponse GetAllBBData()
        {
            try
            {
                loggerManager.LogInformation(new StateLog(GetUserName(), methodName: nameof(GetAllBBData)));

                var data = bBDataService.GetAll();
                loggerManager.LogInformation(new StateLog(GetUserName(), outputParameters: JsonConvert.SerializeObject(data), methodName: nameof(GetAllBBData)));

                return new GetAllBBDataResponse
                {
                    BBData = data,
                    ResponseResult = ResponseResult.ReturnSuccess()
                };
            }
            catch (Exception ex)
            {
                loggerManager.LogInformation(new StateLog(GetUserName(), methodName: nameof(GetAllBBData), message: ex.ToString()));

                return new GetAllBBDataResponse
                {
                    ResponseResult = ResponseResult.ReturnError(ex.Message)
                };
            }
        }

        [HttpPost]
        [Route("GetAllBBDataWithAllRelations")]
        public GetAllBBDataResponse GetAllBBDataWithAllRelations()
        {
            try
            {
                loggerManager.LogInformation(new StateLog(GetUserName(), methodName: nameof(GetAllBBDataWithAllRelations)));

                var data = bBDataService.GetAll(null, true, Constants.BBNumberRelations.Seats, Constants.BBNumberRelations.Materials, Constants.BBNumberRelations.BBDetails);

                loggerManager.LogInformation(new StateLog(GetUserName(), outputParameters: JsonConvert.SerializeObject(data), methodName: nameof(GetAllBBDataWithAllRelations)));

                return new GetAllBBDataResponse
                {
                    BBData = data,
                    ResponseResult = ResponseResult.ReturnSuccess()
                };
            }
            catch (Exception ex)
            {
                loggerManager.LogInformation(new StateLog(GetUserName(), methodName: nameof(GetAllBBDataWithAllRelations), message: ex.ToString()));
                return new GetAllBBDataResponse
                {
                    ResponseResult = ResponseResult.ReturnError(ex.Message)
                };
            }
        }

        [HttpPost]
        [Route("GetAllBBDataWithSeats")]
        public GetAllBBDataResponse GetAllBBDataWithSeats()
        {
            try
            {
                loggerManager.LogInformation(new StateLog(GetUserName(), methodName: nameof(GetAllBBDataWithSeats)));

                var data = bBDataService.GetAll(null, true, Constants.BBNumberRelations.Seats, Constants.BBNumberRelations.BBDetails);
                loggerManager.LogInformation(new StateLog(GetUserName(), outputParameters: JsonConvert.SerializeObject(data), methodName: nameof(GetAllBBDataWithSeats)));

                return new GetAllBBDataResponse
                {
                    BBData = data,
                    ResponseResult = ResponseResult.ReturnSuccess()
                };
            }
            catch (Exception ex)
            {
                loggerManager.LogInformation(new StateLog(GetUserName(), methodName: nameof(GetAllBBDataWithSeats), message: ex.ToString()));
                return new GetAllBBDataResponse
                {
                    ResponseResult = ResponseResult.ReturnError(ex.Message)
                };
            }
        }

        [HttpPost]
        [Route("GetAllBBDataWithSeatsByDate")]
        public GetAllBBDataResponse GetAllBBDataWithSeatsByDate([FromBody] GetAllBBDataRequest request)
        {
            try
            {
                loggerManager.LogInformation(new StateLog(GetUserName(), methodName: nameof(GetAllBBDataWithSeatsByDate), inputParameters:JsonConvert.SerializeObject(request)));

                var bbData = bBDataService.GetAll(
                     s => s.CreatedOn >= request.startDate && s.CreatedOn <= request.endDate,
                     true,
                     Constants.BBNumberRelations.Seats,
                     Constants.BBNumberRelations.BBDetails); 
                loggerManager.LogInformation(new StateLog(GetUserName(), outputParameters: JsonConvert.SerializeObject(bbData), methodName: nameof(GetAllBBDataWithSeats)));

                return new GetAllBBDataResponse
                {
                    BBData = bbData,
                    ResponseResult = ResponseResult.ReturnSuccess()
                };
            }
            catch (Exception ex)
            {
                loggerManager.LogInformation(new StateLog(GetUserName(), methodName: nameof(GetAllBBDataWithSeats), message: ex.ToString()));
                return new GetAllBBDataResponse
                {
                    ResponseResult = ResponseResult.ReturnError(ex.Message)
                };
            }
        }

        [HttpPost]
        [Route("GetAllBBDataWithSeatsByCurrentWeek")]
        public GetAllBBDataResponse GetAllBBDataWithSeatsByCurrentWeek()
        {
            try
            {
                var startDate = DateTime.Now.AddDays(-7);

                var bbData = bBDataService.GetAll(
                     s => s.CreatedOn >= startDate,
                     true,
                     Constants.BBNumberRelations.Seats,
                     Constants.BBNumberRelations.BBDetails);

                loggerManager.LogInformation(new StateLog(GetUserName(), message: "Retrieve from DB", outputParameters: JsonConvert.SerializeObject(bbData), methodName: nameof(GetAllBBDataWithSeatsByCurrentWeek)));

                return new GetAllBBDataResponse
                {
                    BBData = bbData,
                    ResponseResult = ResponseResult.ReturnSuccess()
                };
            }
            catch (Exception ex)
            {
                loggerManager.LogInformation(new StateLog(GetUserName(), message: ex.ToString(), methodName: nameof(GetAllBBDataWithSeatsByCurrentWeek)));
                return new GetAllBBDataResponse
                {
                    ResponseResult = ResponseResult.ReturnError(ex.Message)
                };
            }
        }

        [HttpPost]
        [Route("GetBBDataWithAllRelations")]
        public GetBBDataResponse GetBBNumberWithAllRelations([FromBody] GetBBDataRequest request)
        {
            try
            {
                loggerManager.LogInformation(new StateLog(GetUserName(), inputParameters: JsonConvert.SerializeObject(request), methodName: nameof(GetBBNumberWithAllRelations)));

                var data = bBDataService.Get(p => p.BBNumber == request.BBNumber, true, Constants.BBNumberRelations.Seats, Constants.BBNumberRelations.Materials, Constants.BBNumberRelations.Cushions, Constants.BBNumberRelations.BBDetails);

                loggerManager.LogInformation(new StateLog(GetUserName(), outputParameters: JsonConvert.SerializeObject(data), methodName: nameof(GetBBNumberWithAllRelations)));
                return new GetBBDataResponse
                {
                    BBData = data,
                    ResponseResult = ResponseResult.ReturnSuccess()
                };
            }
            catch (Exception ex)
            {
                loggerManager.LogInformation(new StateLog(GetUserName(), methodName: nameof(GetBBNumberWithAllRelations), message: ex.ToString()));
                return new GetBBDataResponse
                {
                    ResponseResult = ResponseResult.ReturnError(ex.Message)
                };
            }
        }

        [HttpPost]
        [Route("PrintSelectedSeats")]
        public PrintSeatsResponse PrintSelectedSeats([FromBody] PrintSeatsRequest request)
        {
            //todo: will be added printer codes
            try
            {
                var printerStringFormat = configuration["PrinterStringFormat"];
                var printerIPAddress = configuration["PrinterIPAddress"];
                var printerPort = Convert.ToInt32(configuration["PrinterPort"]);
                var userName = GetUserName();

                //loggerManager.LogInformation(new StateLog(GetUserName(), inputParameters: JsonConvert.SerializeObject(request), methodName: nameof(PrintSelectedSeats), message: $"PrinterStringFormat: {printerStringFormat} - PrinterIPAddress: {printerIPAddress} - PrinterPort: {printerPort}"));

                ZebraPrinterHelper zebraPrinterHelper = new ZebraPrinterHelper(printerIPAddress, printerPort, printerStringFormat);

                var groupedData = request.SeatDetails.GroupBy(p => p.BBNumber).Select(p => new { BBNumber = p.FirstOrDefault().BBNumber, Details = p.ToList() }).ToList();

                loggerManager.LogInformation(new StateLog(userName, inputParameters: JsonConvert.SerializeObject(groupedData), methodName: nameof(PrintSelectedSeats), message: $"PrinterStringFormat: {printerStringFormat} - PrinterIPAddress: {printerIPAddress} - PrinterPort: {printerPort}"));

                var printTypeKey = string.Empty;
                if (request.PrintType == (int)GlobalEnums.SeatPrintTypes.BackRest)
                    printTypeKey = Constants.BackRestKey;
                else 
                    printTypeKey= Constants.CushionKey;

                groupedData.ForEach(bbDataItem =>
                {
                    var bbDataForDetails = bBDataService.Get(p => p.BBNumber == bbDataItem.BBNumber, true, Constants.BBNumberRelations.BBDetails);

                    // update and pring seats
                    bbDataItem.Details.ForEach(item =>
                    {
                        item.BBDetail = bbDataForDetails.BBDetails.FirstOrDefault();
                        item.Seat.PrintUser = userName;
                        item.Seat.PrintDate = DateTime.Now;
                        var str = zebraPrinterHelper.GeneratePrinterString(item, printTypeKey);
                        loggerManager.LogInformation(new StateLog(userName, methodName: nameof(PrintSelectedSeats), message: $"Generated Printer String - {item.Seat.Id} : {str}"));
                        zebraPrinterHelper.Print(str);
                        item.Seat.Status = (int)GlobalEnums.StatusCode.Printed;
                        this.seatService.Update(item.Seat);
                        Task.Delay(1000).Wait();
                    });

                    // check for all seats is printed and update bb data as completed
                    var bbData = bBDataService.Get(p => p.BBNumber == bbDataItem.BBNumber, true, Constants.BBNumberRelations.Seats);
                    var isAllSelected = !bbData.Seats.Any(p => p.Status == (int)GlobalEnums.StatusCode.Waiting);
                    if (isAllSelected)
                    {
                        bbData.Status = (int)GlobalEnums.BBDataStatusCode.Completed;
                        bBDataService.Update(bbData);
                    }
                });

                return new PrintSeatsResponse
                {
                    ResponseResult = ResponseResult.ReturnSuccess()
                };
            }
            catch (Exception ex)
            {
                loggerManager.LogInformation(new StateLog(GetUserName(), methodName: nameof(PrintSelectedSeats), message: ex.ToString()));
                return new PrintSeatsResponse
                {
                    ResponseResult = ResponseResult.ReturnError(ex.Message)
                };
            }
        }

        [HttpPost]
        [Route("GetAllFaultyBBs")]
        public GetAllFaultyBBsResponse GetAllFaultyBBs()
        {
            try
            {
                loggerManager.LogInformation(new StateLog(GetUserName(), methodName: nameof(GetAllFaultyBBs)));

                var response = new GetAllFaultyBBsResponse
                {
                    ResponseResult = ResponseResult.ReturnSuccess()
                };
                var faultyBBs = faultyBBService.GetAll()?.OrderByDescending(p => p.CreatedOn);

                foreach (var item in faultyBBs)
                {
                    var bbData = bBDataService.Get(p => p.BBNumber == item.BBNumber, true, Constants.BBNumberRelations.Seats, Constants.BBNumberRelations.BBDetails);
                    response.FaultyBBDetails.Add(new FaultyBBDetail
                    {
                        BBDetail = bbData.BBDetails.FirstOrDefault(),
                        FaultyBB = item,
                        Seat = bbData.Seats.Where(p => p.Id == item.SeatId).FirstOrDefault()
                    });
                }
                loggerManager.LogInformation(new StateLog(GetUserName(), outputParameters: JsonConvert.SerializeObject(response), methodName: nameof(GetAllFaultyBBs)));

                return response;
            }
            catch (Exception ex)
            {
                loggerManager.LogInformation(new StateLog(GetUserName(), methodName: nameof(GetAllFaultyBBs), message: ex.ToString()));
                return new GetAllFaultyBBsResponse
                {
                    ResponseResult = ResponseResult.ReturnError(ex.Message)
                };
            }
        }

        [HttpPost]
        [Route("RepairFaultyBBs")]
        public RepairFaultyBBResponse RepairFaultyBBs([FromBody] RepairFaultyBBRequest request)
        {
            try
            {
                //todo: will be added printer codes
                var userName = GetUserName();
                loggerManager.LogInformation(new StateLog(userName, inputParameters: JsonConvert.SerializeObject(request), methodName: nameof(RepairFaultyBBs)));

                List<Task> taskList = new List<Task>();

                request.FaultyBBs.ForEach(item =>
                {
                    var updateTask = new Task(() =>
                    {
                        var data = this.faultyBBService.Get(p => p.Id == item.Id);
                        data.RepairedBy = userName;
                        data.RepairedDate = DateTime.Now;
                        data.Status = (int)GlobalEnums.StatusCode.Repaired;
                        this.faultyBBService.Update(data);
                    });
                    taskList.Add(updateTask);
                    updateTask.Start();
                });
                Task.WaitAll(taskList.ToArray());

                return new RepairFaultyBBResponse
                {
                    ResponseResult = ResponseResult.ReturnSuccess()
                };
            }
            catch (Exception ex)
            {
                loggerManager.LogInformation(new StateLog(GetUserName(), methodName: nameof(RepairFaultyBBs), message: ex.ToString()));
                return new RepairFaultyBBResponse
                {
                    ResponseResult = ResponseResult.ReturnError(ex.Message)
                };
            }
        }

        [HttpPost]
        [Route("GetAllPairingHistory")]
        public GetAllPairingHistoryResponse GetAllPairingHistory()
        {
            try
            {
                loggerManager.LogInformation(new StateLog(GetUserName(), methodName: nameof(GetAllPairingHistory)));

                var data = pairingHistoryService.GetAll();

                var response = new GetAllPairingHistoryResponse
                {
                    PairingHistory = data?.OrderByDescending(p => p.CreatedOn).ToList(),
                    ResponseResult = ResponseResult.ReturnSuccess()
                };
                loggerManager.LogInformation(new StateLog(GetUserName(), outputParameters: JsonConvert.SerializeObject(response), methodName: nameof(GetAllPairingHistory)));
                return response;
            }
            catch (Exception ex)
            {
                loggerManager.LogInformation(new StateLog(GetUserName(), methodName: nameof(GetAllPairingHistory), message: ex.ToString()));
                return new GetAllPairingHistoryResponse
                {
                    ResponseResult = ResponseResult.ReturnError(ex.Message)
                };
            }
        }

        [HttpPost]
        [Route("SetPairingHistory")]
        public SetPairingHistoryResponse SetPairingHistory([FromBody] SetPairingHistoryRequest request)
        {
            try
            {
                loggerManager.LogInformation(new StateLog(GetUserName(), methodName: nameof(SetPairingHistory), inputParameters: JsonConvert.SerializeObject(request)));

                var pairingHistoryData = pairingHistoryService.Get(p => p.BBNumber == request.BBNumber);
                if (pairingHistoryData != null)
                    return new SetPairingHistoryResponse
                    {
                        ResponseResult = ResponseResult.ReturnError("BB Numarası birden fazla eşleştirmede kullanılamaz.")
                    };

                // if barcode numbers not equal eachother send email for pairing information
                if(request.BBNumber != request.TransportNumber)
                {
                    var emailBody = new StringBuilder();
                    emailBody.AppendLine("<p>Merhaba,</p>");
                    emailBody.AppendLine($"<p>{DateTime.Now.ToString("dd/MM/yyyy")} tarihinde {request.TransportNumber} no.lu araç ile {request.BBNumber} no.lu araç {GetUserName()} kullanıcısı tarafından eşleştirilmiştir..</p>");
                    emailBody.AppendLine("<p>Kontrolünün sağlanması ve gerekli değişikliklerin yapılması adına bilgilerinize.</p>");
                    emailBody.AppendLine("<p>İlgili rapora aşağıda yer alan link üzerinden ulaşabilirsiniz.</p>");
                    emailBody.AppendLine($"<p><a href={configuration["UIUrl"]}>Rapor Link</a></p>");

                    _mailService.SendEmail(new MailRequest(emailBody.ToString()));

                    loggerManager.LogInformation(new StateLog(GetUserName(), methodName: "MailInformation", inputParameters: $"{request.TransportNumber} - {request.BBNumber}"));
                }

                pairingHistoryService.Add(new PairingHistory
                {
                    BBNumber = request.BBNumber,
                    TransportNumber = request.TransportNumber,
                    Status = request.Status,
                });

                return new SetPairingHistoryResponse
                {
                    ResponseResult = ResponseResult.ReturnSuccess()
                };
            }
            catch (Exception ex)
            {
                loggerManager.LogInformation(new StateLog(GetUserName(), methodName: nameof(SetPairingHistory), message: ex.ToString()));
                return new SetPairingHistoryResponse
                {
                    ResponseResult = ResponseResult.ReturnError(ex.Message)
                };
            }
        }
    }
}
