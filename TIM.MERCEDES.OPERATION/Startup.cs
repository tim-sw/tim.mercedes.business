using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http.Features;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TIM.MERCEDES.BUSINESS.Abstract;
using TIM.MERCEDES.BUSINESS.Concrete;
using TIM.MERCEDES.DATAACCESS.Abstract;
using TIM.MERCEDES.DATAACCESS.Concrete.EntityFramework;
using TIM.MERCEDES.ENTITIES.Concrete.Settings;
using TIM.MERCEDES.LOGGER.Abstract;
using TIM.MERCEDES.LOGGER.Concrete.Log4Net;

namespace TIM.MERCEDES.OPERATION
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors(options =>
            {
                options.AddDefaultPolicy(
                builder =>
                {
                    builder
                    .SetIsOriginAllowed(origin => true) // allow any origin
                    .AllowAnyHeader()
                    .AllowAnyMethod()
                    .AllowCredentials();
                });
            });

            services.Configure<MailSettings>(Configuration.GetSection("MailSettings"));

            services.AddHttpContextAccessor();
            services.AddTransient<IBBDataDAL, BBDataDAL>();
            services.AddTransient<ISeatDAL, SeatDAL>();
            services.AddTransient<IFaultyBBDAL, FaultyBBDAL>();
            services.AddTransient<IMaterialDAL, MaterialDAL>();
            services.AddTransient<ICushionDAL, CushionDAL>();
            services.AddTransient<IBBDetailDAL, BBDetailDAL>();
            services.AddTransient<IPairingHistoryDAL, PairingHistoryDAL>();
            services.AddTransient<ILoggerManager, Log4NetManager>();
            services.AddTransient<IMailService, MailService>();
            services.AddControllers();
            services.AddControllersWithViews().AddNewtonsoftJson(options =>
                options.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore
            );
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseRouting();

            app.UseCors();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
