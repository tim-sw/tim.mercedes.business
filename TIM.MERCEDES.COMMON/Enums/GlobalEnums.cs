﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TIM.MERCEDES.COMMON.Enums
{
    public class GlobalEnums
    {
        public enum StatusCode
        {
            Printed = 1,
            Waiting = 2,
            Repaired = 3,
        }

        public enum BBDataStatusCode
        {
            Waiting = 1,
            Completed = 2,
        }

        public enum FaultyBBDataStatusCode
        {
            Faulty = 1,
            Damaged = 2,
            Repaired = 3,
            ReProduction = 4,
        }
        public enum PairingHistoryStatusCode
        {
            CorrectPairing = 1,
            UserApproved = 2,
        }

        public enum TypeCode
        {
            Curtaion = 1,
            Passenger_Seat = 2,
            Stewardess_Seat = 3,
            Driver_Seat = 4
        }

        public enum SeatPrintTypes
        {
            Cushion = 1,
            BackRest = 2
        }
    }
}
