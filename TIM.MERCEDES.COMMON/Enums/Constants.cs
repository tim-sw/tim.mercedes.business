﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TIM.MERCEDES.COMMON.Enums
{
    public static class Constants
    {
        public const string ServicePath = "api";
        public const string ZebraPrinterNewLine = "\\0D\\0A";
        public const string CacheKeyForWeeklyData = "MERCEDES_IsWeeklyDateRetrieved";

        public const string BackRestKey = "Sırtlık";
        public const string CushionKey = "Minder";
        public static class BBNumberRelations
        {
            public const string Seats = "Seats";
            public const string BBDetails = "BBDetails";
            public const string Materials = "Materials";
            public const string Cushions = "Cushions";
        }
        public static class FaultyBBRelations
        {
            public const string Seat = "Seat";
            public const string BBDetail = "BBDetail";
        }
    }
}
