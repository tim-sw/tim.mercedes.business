﻿using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using TIM.MERCEDES.COMMON.Models;

namespace TIM.MERCEDES.COMMON.Executions
{
    public class APIExecution<T> : IDisposable
    {
        public T CallAPI(string Url, HttpCallMethod httpMethod, Dictionary<string, string> headers = null, Dictionary<string, string> parameters = null, object jsonBody = null, bool ignoreNull = false, Dictionary<string, string> cookies = null)
        {
            T responseObject = default(T);

            try
            {
                var client = new RestClient(Url);
                var request = new RestRequest((Method)httpMethod);
                if (headers != null)
                {
                    foreach (var itemHeader in headers)
                    {
                        request.AddHeader(itemHeader.Key, itemHeader.Value);
                    }
                }
                if (parameters != null)
                {
                    foreach (var itemParameter in parameters)
                    {
                        request.AddParameter(itemParameter.Key, itemParameter.Value);
                    }
                }
                if (cookies != null)
                {
                    foreach (var itemCookie in cookies)
                    {
                        request.AddCookie(itemCookie.Key, itemCookie.Value);
                    }
                }
                if (jsonBody != null)
                {
                    request.AddJsonBody(jsonBody, "application/json");
                }
                IRestResponse response = client.Execute(request);
                {
                    if (response.StatusCode == HttpStatusCode.OK)
                    {
                        responseObject = JsonConvert.DeserializeObject<T>(response.Content);
                    }
                    else
                        throw new Exception(response.StatusDescription);
                }

            }
            catch (Exception ex)
            {

            }
            return responseObject;
        }
        public async Task<T> CallAPIAsync(string Url, HttpCallMethod httpMethod, Dictionary<string, string> headers = null, Dictionary<string, string> parameters = null, object jsonBody = null, bool ignoreNull = false, Dictionary<string, string> cookies = null)
        {
            T responseObject = default(T);

            try
            {
                var client = new RestClient(Url);
                var request = new RestRequest((Method)httpMethod);
                if (headers != null)
                {
                    foreach (var itemHeader in headers)
                    {
                        request.AddHeader(itemHeader.Key, itemHeader.Value);
                    }
                }
                if (parameters != null)
                {
                    foreach (var itemParameter in parameters)
                    {
                        request.AddParameter(itemParameter.Key, itemParameter.Value);
                    }
                }
                if (cookies != null)
                {
                    foreach (var itemCookie in cookies)
                    {
                        request.AddCookie(itemCookie.Key, itemCookie.Value);
                    }
                }
                if (jsonBody != null)
                {
                    request.AddJsonBody(jsonBody, "application/json");
                }
                IRestResponse response = await client.ExecuteAsync(request);
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    responseObject = JsonConvert.DeserializeObject<T>(response.Content);
                }
                else
                    throw new Exception(response.StatusDescription);

            }
            catch (Exception ex)
            {

            }
            return responseObject;
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
    }
   
}
