﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Serialization;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using TIM.MERCEDES.ENTITIES.Concrete;
using TIM.MERCEDES.ENTITIES.Concrete.Models;
using TIM.MERCEDES.LOGGER.Abstract;

namespace TIM.MERCEDES.COMMON.Helpers
{
    public class ServiceHelper
    {
        private string apiUrl { get; set; }
        private ILoggerManager loggerManager;

        public ServiceHelper(string _apiUrl)
        {
            apiUrl = _apiUrl;
        }

        public ServiceHelper(string _apiUrl, ILoggerManager _loggerManager)
        {
            apiUrl = _apiUrl;
            loggerManager = _loggerManager;
        }

        public GetBBDataFromS2GatherResponse GetBBData(DateTime startDate, DateTime endDate)
        {
            try
            {
                var queryUrl = $"{apiUrl}/GetRangeBBList/?SDate={startDate.ToString("yyyy-MM-dd")}&FDate={endDate.ToString("yyyy-MM-dd")}";
                var restClient = new RestClient(queryUrl);
                var request = new RestRequest(Method.GET);
                IRestResponse serviceResponse = restClient.Execute(request);
                var response = new GetBBDataFromS2GatherResponse();
                loggerManager.LogInformation(new StateLog(string.Empty, inputParameters: queryUrl, outputParameters: JsonConvert.SerializeObject(response), methodName: "GetRangeBBList"));
                if (serviceResponse.StatusCode == System.Net.HttpStatusCode.OK && serviceResponse.Content != null)
                {
                    return new GetBBDataFromS2GatherResponse
                    {
                        BBData = JsonConvert.DeserializeObject<List<BBData>>(serviceResponse.Content),
                        ResponseResult = ResponseResult.ReturnSuccess()
                    };
                }
                return new GetBBDataFromS2GatherResponse
                {
                    ResponseResult = ResponseResult.ReturnError("Null Content")
                };
            }
            catch (Exception ex)
            {
                loggerManager.LogInformation(new StateLog(string.Empty, message: ex.ToString(), methodName: "GetRangeBBList"));
                return new GetBBDataFromS2GatherResponse
                {
                    ResponseResult = ResponseResult.ReturnError(ex.Message)
                };
            }
        }

        public GetBBNumberDetailsFromS2GatherResponse GetBBNumberDetails(string bbNumber)
        {
            try
            {
                var queryUrl = $"{apiUrl}/GetFullSeatPlacementList/?BBNumber={bbNumber}";

                var restClient = new RestClient(queryUrl);
                var request = new RestRequest(Method.GET);
                IRestResponse serviceResponse = restClient.Execute(request);
                loggerManager.LogInformation(new StateLog(string.Empty, inputParameters: queryUrl, outputParameters: JsonConvert.SerializeObject(serviceResponse.Content), methodName: "GetFullSeatPlacementList"));

                if (serviceResponse.StatusCode == System.Net.HttpStatusCode.OK && serviceResponse.Content != null)
                {
                    var response = JsonConvert.DeserializeObject<GetBBNumberDetailsFromS2GatherResponse>(serviceResponse.Content, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore });
                    response.ResponseResult = ResponseResult.ReturnSuccess();
                    return response;
                }
                return new GetBBNumberDetailsFromS2GatherResponse
                {
                    ResponseResult = ResponseResult.ReturnError("Null Content")
                };
            }
            catch (Exception ex)
            {
                loggerManager.LogInformation(new StateLog(string.Empty, message: ex.ToString(), methodName: "GetFullSeatPlacementList"));

                return new GetBBNumberDetailsFromS2GatherResponse
                {
                    ResponseResult = ResponseResult.ReturnError(ex.Message)
                };
            }

        }
    }
}
