﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.DirectoryServices.AccountManagement;
using System.Text;
using TIM.MERCEDES.ENTITIES.Concrete;
using TIM.MERCEDES.LOGGER.Abstract;

namespace TIM.MERCEDES.COMMON.Helpers
{
    public class AuthenticationHelper
    {
        private string LDAP_DOMAIN { get; set; }
        private int LDAP_PORT { get; set; }

        private ILoggerManager loggerManager;
        public AuthenticationHelper(string _ldapDomain, int _ldapPort, ILoggerManager _loggerManager = null)
        {
            LDAP_DOMAIN = _ldapDomain;
            LDAP_PORT = _ldapPort;
            loggerManager = _loggerManager;
        }

        public bool AuthenticateWithLDAP(string _userName, string _password)
        {
            try
            {
                using (var pc = new PrincipalContext(ContextType.Domain, $"{LDAP_DOMAIN}:{LDAP_PORT}"))
                {
                    loggerManager.LogInformation(new StateLog(_userName, inputParameters: JsonConvert.SerializeObject(new { userName = _userName, Password = _password }), methodName: "AuthenticateWithLDAP"));

                    var res = pc.ValidateCredentials(_userName, _password);
                    loggerManager.LogInformation(new StateLog(_userName, outputParameters: res.ToString(), methodName: "AuthenticateWithLDAP"));

                    return res;
                }
            }
            catch (Exception ex)
            {
                loggerManager.LogInformation(ex.Message);
                return false;
            }
        }
    }
}
