﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TIM.MERCEDES.COMMON.Enums;
using TIM.MERCEDES.ENTITIES.Concrete.Models;

namespace TIM.MERCEDES.COMMON.Helpers
{
    public class ZebraPrinterHelper
    {
        private string printerIpAddress { get; set; }
        private int printerPort { get; set; }
        private string printerStringFormat { get; set; }

        public ZebraPrinterHelper(string _printerIpAddress, int _printerPort, string _printerStringFormat)
        {
            printerIpAddress = _printerIpAddress;
            printerPort = _printerPort;
            printerStringFormat = _printerStringFormat;
        }

        public ZebraPrinterHelper(string _printerIpAddress, int _printerPort)
        {
            printerIpAddress = _printerIpAddress;
            printerPort = _printerPort;
        }

        public void Print(string printerStr)
        {
            try
            {
                // Open connection
                System.Net.Sockets.TcpClient client = new System.Net.Sockets.TcpClient();
                client.Connect(printerIpAddress, printerPort);
                // Write ZPL String to connection
                System.IO.StreamWriter writer =
                new System.IO.StreamWriter(client.GetStream());
                writer.Write(printerStr);
                writer.Flush();
                // Close Connection
                writer.Close();
                client.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string GeneratePrinterString(string generatedBarcode, string bbNumber, string seatCode, string seatTypeCode)
        {
            if (!string.IsNullOrEmpty(this.printerStringFormat))
            {
                return this.printerStringFormat
                        .Replace("$BARCODE", generatedBarcode)
                        .Replace("$BB_NUMBER", bbNumber)
                        .Replace("$SEAT_CODE", seatCode)
                        .Replace("$SEAT_TYPE_CODE", seatTypeCode);
            }
            return string.Empty;
            //return "^XA ~TA000 ~JSN ^LT0 ^MNN ^MTT ^PON ^PMN ^LH0,0 ^JMA ^PR3,3 ~SD28 ^JUS ^LRN ^CI27 ^PA0,1,1,0 ^XZ ^XA ^MMT ^PW203 ^LL519 ^LS0 ^FO1,5^GB167,504,1^FS ^FO1,315^GB167,0,1^FS ^FO1,81^GB167,0,1^FS ^FO139,82^GB0,234,1^FS ^FO111,82^GB0,234,1^FS ^FO83,82^GB0,234,1^FS ^FO28,82^GB0,234,1^FS ^FO56,82^GB0,234,1^FS ^FT16,512^BQN,2,5 ^FH\\^FDLA,"
            //    + generatedBarcode +
            //    "^FS ^FPH,5^FT145,133^A@R,17,13,TT0003M_^FB148,1,9,C^FH\\^CI28^FD"
            //    + productNumber +
            //    "^FS^CI27 ^FPH,5^FT114,110^A@R,17,13,TT0003M_^FB181,1,9,C^FH\\^CI28^FD"
            //    + productName +
            //    "^FS^CI27 ^FPH,5^FT88,94^A@R,17,13,TT0003M_^FB204,1,9,C^FH\\^CI28^FD"
            //    + productSerial +
            //    "^FS^CI27 ^FPH,5^FT65,189^A@R,17,13,TT0003M_^FB26,1,9,C^FH\\^CI28^FD"
            //    + productAdress +
            //    "^FS^CI27 ^FPH,5^FT34,145^A@R,17,13,TT0003M_^FB134,1,9,C^FH\\^CI28^FD"
            //    + prodcutMaterial +
            //    "^FS^CI27 ^FPH,5^FT8,150^A@R,15,13,TT0003M_^FB114,1,8,C^FH\\^CI28^FD"
            //    + date +
            //    "^FS^CI27 ^PQ1,0,1,Y ^XZ";
        }

        public string GeneratePrinterString(SeatDetail seatDetail, string printTypeKey)
        {
            if (!string.IsNullOrEmpty(this.printerStringFormat))
            {
                var formattedBarCodeString = new StringBuilder();
                formattedBarCodeString.AppendLine($"{seatDetail.BBDetail.BBDataId};{seatDetail.Seat.Id};{seatDetail.BBNumber};");
                //formattedBarCodeString.Append($"{Constants.ZebraPrinterNewLine} {Constants.ZebraPrinterNewLine}");
                //seatDetail.Materials.ForEach(item =>
                //{
                //    formattedBarCodeString.AppendLine($"{item.MaterialNumber} / {item.Group}");
                //    formattedBarCodeString.Append($"{Constants.ZebraPrinterNewLine} {Constants.ZebraPrinterNewLine}");
                //});
                return this.printerStringFormat
                        .Replace("@BARCODE_STRING", formattedBarCodeString.ToString())
                        .Replace("@BBNUMBER", seatDetail.BBNumber)
                        .Replace("@SEAT_CODE", seatDetail.Seat.SeatCode)
                        .Replace("@SEAT_TYPE", seatDetail.BBDetail.SeatType)
                        .Replace("@PRINT_TYPE", printTypeKey.ToUpper());
            }
            return string.Empty;
        }
    }
}
