﻿using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Text;

namespace TIM.MERCEDES.COMMON.Helpers
{
    public class RedisHelper
    {
        private string serviceUrl { get; set; }
        public RedisHelper(string _serviceUrl)
        {
            serviceUrl = _serviceUrl;
        }

        public bool SaveCache(string key, string value, DateTime? expDate = null)
        {
            try
            {
                bool isSuccess = false;
                using (var redisConnection = ConnectionMultiplexer.Connect(serviceUrl))
                {
                    var redisCache = redisConnection.GetDatabase();
                    if (string.IsNullOrEmpty(redisCache.StringGet(key)))
                    {
                        if (expDate != null)
                        {
                            var expiryTimeSpan = expDate.Value.Subtract(DateTime.Now);
                            isSuccess = redisCache.StringSet(key, value, expiryTimeSpan);
                        }
                        else
                        {
                            isSuccess = redisCache.StringSet(key, value);
                        }
                    }

                }
                return isSuccess;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string GetCache(string key)
        {
            try
            {
                using (var redisConnection = ConnectionMultiplexer.Connect(serviceUrl))
                {
                    var redisCache = redisConnection.GetDatabase();

                    var valueString = redisCache.StringGet(key);

                    return valueString;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public void DeleteCache(string key)
        {
            try
            {
                using (var redisConnection = ConnectionMultiplexer.Connect(serviceUrl))
                {
                    var redisCache = redisConnection.GetDatabase();

                    redisCache.KeyDelete(key);
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
    }
}
