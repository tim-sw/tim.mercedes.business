﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TIM.MERCEDES.COMMON.Helpers
{
    public class TypeCodeHelper
    {
        private static readonly List<TypeCode> typeCodes = new List<TypeCode>
        {
            new TypeCode{ Label = "Perde", Value = 1},
            new TypeCode{ Label = "Yolcu Koltuğu", Value = 2},
            new TypeCode{ Label = "Hostes Koltuğu", Value = 3},
            new TypeCode{ Label = "Şöför Koltuğu", Value = 4},
        };
    }

    internal class TypeCode
    {
        public int Value { get; set; }
        public string Label { get; set; }
    }
}
