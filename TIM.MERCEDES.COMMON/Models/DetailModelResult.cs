﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TIM.MERCEDES.COMMON.Models
{
    public class DetailModelResult<T> : IDisposable
    {
        public OperationStatus Status { get; set; }
        public string Message { get; set; }
        public T Detail { get; set; }
        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
    }
}
