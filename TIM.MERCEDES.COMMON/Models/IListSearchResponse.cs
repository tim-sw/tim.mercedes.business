﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TIM.MERCEDES.COMMON.Models
{
    public interface IListSearchResponse
    {
        public int Offset { get; set; }
        public int Total { get; set; }
        public int TotalNotFiltered { get; set; }
    }
}
