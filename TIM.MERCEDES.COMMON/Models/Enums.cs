﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TIM.MERCEDES.COMMON.Models
{
    public enum OperationStatus
    {
        Failed = 1,
        PartiallySuccessful = 2,
        Successful = 3,
        NotFound = 4
    }
    public enum HttpCallMethod
    {
        GET = 0,
        POST = 1,
        PUT = 2,
        DELETE = 3,
        HEAD = 4,
        OPTIONS = 5,
        PATCH = 6,
        MERGE = 7,
        COPY = 8
    }
}
