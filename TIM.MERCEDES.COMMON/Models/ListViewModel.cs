﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TIM.MERCEDES.COMMON.Models
{
    public class ListViewModel<T> : IListSearchResponse, IDisposable
    {
        public List<T> Rows { get; set; }
        public int Offset { get; set; }
        public int Total { get; set; }
        public int TotalNotFiltered { get; set; }
        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
    }
}
