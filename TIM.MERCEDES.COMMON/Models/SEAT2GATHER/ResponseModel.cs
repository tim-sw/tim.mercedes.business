﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TIM.MERCEDES.COMMON.Models.SEAT2GATHER
{

    public class Rootobject
    {
        public Item1[] Item1 { get; set; }
        public Item2[] Item2 { get; set; }
    }

    public class Item1
    {
        public int Id { get; set; }
        public string BBNumber { get; set; }
        public string MaterialNumber { get; set; }
        public string SeatCode { get; set; }
        public string SeatCodeTyp { get; set; }
        public int SeatCodeNo { get; set; }
        public int Quantity { get; set; }
        public string OrderNumber { get; set; }
    }

    public class Item2
    {
        public int Id { get; set; }
        public string BBNumber { get; set; }
        public string OrderNumber { get; set; }
        public string MaterialNumber { get; set; }
        public string SeatCode { get; set; }
        public string SeatCodeTyp1 { get; set; }
        public int Quantity { get; set; }
        public int SeatCodeNo { get; set; }
    }

}
