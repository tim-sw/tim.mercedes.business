﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json.Serialization;
using TIM.MERCEDES.ENTITIES.Abstract;

namespace TIM.MERCEDES.ENTITIES.Concrete
{
    public class Material : IEntity
    {
        public int Id { get; set; }
        public int BBDataId { get; set; }
        public string S2GetherId { get; set; }
        public string OrderNumber { get; set; }
        public string MaterialNumber { get; set; }
        [JsonPropertyName("Naming")]
        public string Name { get; set; }
        public string Description { get; set; }
        public int GroupName { get; set; }
        public string Group { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime ModifiedOn { get; set; }
        public string CreatedBy { get; set; }
        public string ModifiedBy { get; set; }
    }
}
