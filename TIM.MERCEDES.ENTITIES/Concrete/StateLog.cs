﻿using System;
using System.Collections.Generic;
using System.Text;
using TIM.MERCEDES.ENTITIES.Abstract;

namespace TIM.MERCEDES.ENTITIES.Concrete
{
    public class StateLog
    {
        public StateLog()
        {
        }
        public StateLog(string userName, string methodName, string deviceId = null, string inputParameters = null, string outputParameters = null, string message = null)
        {
            this.UserName = userName;
            this.DeviceId = deviceId;
            this.InputParameters = inputParameters;
            this.OutputParameters = outputParameters;
            this.MethodName = methodName;
            this.Message = message;
        }
        public int Id { get; set; }
        public string UserName { get; set; }
        public string DeviceId { get; set; }
        public string InputParameters { get; set; }
        public string OutputParameters { get; set; }
        public string MethodName { get; set; }
        public string Message { get; set; }
        public DateTime CreatedOn { get; set; }
    }
}
