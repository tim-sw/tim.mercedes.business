﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using TIM.MERCEDES.ENTITIES.Abstract;

namespace TIM.MERCEDES.ENTITIES.Concrete
{
    public class Seat : IEntity
    {
        public int Id { get; set; }
        public int BBDataId { get; set; }
        public int S2GetherId { get; set; }
        public string OrderNumber { get; set; }
        public string SeatCodeType { get; set; }
        public string SeatCode { get; set; }
        [DefaultValue(2)]
        public int Status { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime ModifiedOn { get; set; }
        public DateTime PrintDate { get; set; }
        public string PrintUser { get; set; }
        public string CreatedBy { get; set; }
        public string ModifiedBy { get; set; }
    }
}
