﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using TIM.MERCEDES.ENTITIES.Abstract;

namespace TIM.MERCEDES.ENTITIES.Concrete
{
    public class BBDetail : IEntity
    {
        public int Id { get; set; }
        public int BBDataId { get; set; }
        public string Explanation1 { get; set; }
        public string Explanation2 { get; set; }
        public string Explanation3 { get; set; }
        public int SeatTypeId { get; set; }
        public string SeatType { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime ModifiedOn { get; set; }
        public string CreatedBy { get; set; }
        public string ModifiedBy { get; set; }

    }
}
