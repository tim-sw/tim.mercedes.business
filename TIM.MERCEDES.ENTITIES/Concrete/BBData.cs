﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using TIM.MERCEDES.ENTITIES.Abstract;

namespace TIM.MERCEDES.ENTITIES.Concrete
{
    public class BBData : IEntity
    {
        public int Id { get; set; }
        public string BBNumber { get; set; }
        [DefaultValue(1)]
        public int Status { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime ModifiedOn { get; set; }
        public string CreatedBy { get; set; }
        public string ModifiedBy { get; set; }

        [ForeignKey("BBDataId")]
        public virtual ICollection<BBDetail> BBDetails { get; set; }
        [ForeignKey("BBDataId")]
        public virtual ICollection<FaultyBB> FaultyBBs { get; set; }
        [ForeignKey("BBDataId")]
        public virtual ICollection<Seat> Seats { get; set; }
        [ForeignKey("BBDataId")]
        public virtual ICollection<Cushion> Cushions { get; set; }
        [ForeignKey("BBDataId")]
        public virtual ICollection<Material> Materials { get; set; }
    }
}
