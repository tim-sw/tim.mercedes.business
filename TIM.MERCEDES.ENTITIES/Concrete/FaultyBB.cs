﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using TIM.MERCEDES.ENTITIES.Abstract;

#nullable disable

namespace TIM.MERCEDES.ENTITIES.Concrete
{
    public class FaultyBB : IEntity
    {
        public int Id { get; set; }
        public int BBDataId { get; set; }
        public string BBNumber { get; set; }
        public int SeatId { get; set; }
        public string Description { get; set; }
        public DateTime? Date { get; set; }
        public string ImgName { get; set; }
        [DefaultValue(2)]
        public int Status { get; set; }
        public bool ReProductionStatus { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime ModifiedOn { get; set; }
        public DateTime RepairedDate { get; set; }
        public string RepairedBy { get; set; }
        public string CreatedBy { get; set; }
        public string ModifiedBy { get; set; }
    }
}
