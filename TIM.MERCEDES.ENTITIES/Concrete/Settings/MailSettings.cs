﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TIM.MERCEDES.ENTITIES.Concrete.Settings
{
    public class MailSettings
    {
        public string FromEmail { get; set; }
        public string Subject { get; set; }
        public List<string> ToEmails { get; set; }
        public List<string> CcEmails { get; set; }
        public string DisplayName { get; set; }
        public string Password { get; set; }
        public string Host { get; set; }
        public int Port { get; set; }
    }
}
