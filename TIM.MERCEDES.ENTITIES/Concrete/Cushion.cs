﻿using System;
using System.Collections.Generic;
using System.Text;
using TIM.MERCEDES.ENTITIES.Abstract;

namespace TIM.MERCEDES.ENTITIES.Concrete
{
    public class Cushion : IEntity
    {
        public int Id { get; set; }
        public int BBDataId { get; set; }
        public string S2GetherId { get; set; }
        public string OrderNumber { get; set; }
        public string MaterialNumber { get; set; }
        public string MaterialName { get; set; }
        public decimal Quantity { get; set; }
        public string Unit { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime ModifiedOn { get; set; }
        public string CreatedBy { get; set; }
        public string ModifiedBy { get; set; }
    }
}
