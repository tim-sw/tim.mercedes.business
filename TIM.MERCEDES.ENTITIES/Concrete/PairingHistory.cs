﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using TIM.MERCEDES.ENTITIES.Abstract;

namespace TIM.MERCEDES.ENTITIES.Concrete
{
    public class PairingHistory : IEntity
    {
        public int Id { get; set; }
        public string TransportNumber { get; set; }
        public string BBNumber { get; set; }
        [DefaultValue(1)]
        public int Status { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime ModifiedOn { get; set; }
        public string CreatedBy { get; set; }
        public string ModifiedBy { get; set; }
    }
}
