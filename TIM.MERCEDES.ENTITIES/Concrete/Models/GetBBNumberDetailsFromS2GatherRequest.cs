﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TIM.MERCEDES.ENTITIES.Concrete.Models
{
    public class GetBBNumberDetailsFromS2GatherRequest: RequestBase
    {
        public string BBNumber { get; set; }
    }
}
