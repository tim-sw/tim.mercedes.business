﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TIM.MERCEDES.ENTITIES.Concrete.Models
{
    public class LoginRequest
    {
        public string userName { get; set; }
        public string password { get; set; }
    }
}
