﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Text.Json.Serialization;

namespace TIM.MERCEDES.ENTITIES.Concrete.Models
{
    public class GetBBNumberDetailsFromS2GatherResponse : ResponseBase
    {
        [Description("This field for BB Detail List")]
        public List<BBDetail> Item1 { get; set; }
        [Description("This field for BB Material List")]
        public List<Material> Item2 { get; set; }
        [Description("This field for BB Cushion List")]
        public List<Cushion> Item3 { get; set; }
        [Description("This field for BB Seat List")]
        public List<Seat> Item4 { get; set; }
    }
}
