﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TIM.MERCEDES.ENTITIES.Concrete.Models
{
    public class GetBBDataRequest: RequestBase
    {
        public string BBNumber { get; set; }
    }
}
