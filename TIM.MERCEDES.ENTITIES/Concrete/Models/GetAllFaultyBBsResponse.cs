﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TIM.MERCEDES.ENTITIES.Concrete.Models
{
    public class GetAllFaultyBBsResponse : ResponseBase
    {
        public List<FaultyBBDetail> FaultyBBDetails { get; set; } = new List<FaultyBBDetail>();
    }

    public class FaultyBBDetail
    {
        public FaultyBB FaultyBB { get; set; }
        public BBDetail BBDetail { get; set; }
        public Seat Seat { get; set; }
    }
}
