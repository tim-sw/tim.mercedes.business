﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TIM.MERCEDES.ENTITIES.Concrete.Models
{
    public class PrintSeatsRequest: RequestBase
    {
        public List<SeatDetail> SeatDetails { get; set; }
        public int PrintType { get; set; }
    }
    public class SeatDetail
    {
        public Seat Seat { get; set; }
        public BBDetail BBDetail { get; set; }
        public List<Cushion> Cushions { get; set; }
        public List<Material> Materials { get; set; }
        public string BBNumber { get; set; }
    }
}
