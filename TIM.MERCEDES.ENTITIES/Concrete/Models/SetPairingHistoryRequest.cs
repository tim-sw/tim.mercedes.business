﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TIM.MERCEDES.ENTITIES.Concrete.Models
{
    public class SetPairingHistoryRequest: RequestBase
    {
        public string BBNumber { get; set; }
        public string TransportNumber { get; set; }
        public int Status { get; set; }
    }
}
