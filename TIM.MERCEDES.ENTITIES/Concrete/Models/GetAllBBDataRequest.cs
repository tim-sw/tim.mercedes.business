﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TIM.MERCEDES.ENTITIES.Concrete.Models
{
    public class GetAllBBDataRequest: RequestBase
    {
        public DateTime startDate { get; set; }
        public DateTime endDate { get; set; }
    }
}
