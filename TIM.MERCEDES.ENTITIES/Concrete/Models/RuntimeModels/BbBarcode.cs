﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TIM.MERCEDES.ENTITIES.Concrete.Models.RuntimeModels
{
    public class BbBarcode
    {
        public string Werk { get; set; }
        public string BBNR { get; set; }
    }
}
