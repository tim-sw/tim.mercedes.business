﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TIM.MERCEDES.ENTITIES.Concrete.Models
{
    public class CacheInformation
    {
        public bool IsDataRetrived { get; set; }
        public DateTime RetrivedDate { get; set; }
    }
}
