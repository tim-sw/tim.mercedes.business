﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TIM.MERCEDES.ENTITIES.Concrete.Models
{
    public class GetAllPairingHistoryResponse : ResponseBase
    {
        public List<PairingHistory> PairingHistory { get; set; }
    }
}
