﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TIM.MERCEDES.ENTITIES.Concrete.Models
{
    public class GetBBDataFromS2GatherResponse : ResponseBase
    {
        public List<BBData> BBData { get; set; }
    }
}
