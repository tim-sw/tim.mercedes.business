﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TIM.MERCEDES.ENTITIES.Concrete.Models
{
    public class GetBBDataResponse : ResponseBase
    {
        public BBData BBData { get; set; }
    }
}
