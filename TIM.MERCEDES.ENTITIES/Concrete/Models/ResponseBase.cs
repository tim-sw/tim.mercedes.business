﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TIM.MERCEDES.ENTITIES.Concrete.Models
{
    public class ResponseBase
    {
        public ResponseResult ResponseResult { get; set; }
    }

    public class ResponseResult
    {

        public bool Result { get; set; }

        public String ExceptionDetail { get; set; }

        public string Id { get; set; }

        public static ResponseResult ReturnSuccess()
        {
            return new ResponseResult { Result = true };
        }

        public static ResponseResult ReturnError(String Detail)
        {
            return new ResponseResult { Result = false, ExceptionDetail = Detail };
        }

        public static ResponseResult ReturnSuccessWithId(string id)
        {
            return new ResponseResult { Result = true, Id = id };
        }

    }
}
