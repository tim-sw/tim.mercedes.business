﻿using MimeKit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TIM.MERCEDES.ENTITIES.Concrete.Models
{
    public class MailRequest
    {
        public List<MailboxAddress> ToEmails { get; }
        public List<MailboxAddress> CcEmails { get; }
        public string Subject { get;}
        public string Body { get;}

        public MailRequest(IEnumerable<string> toEmails, IEnumerable<string> ccEmails, string subject, string body)
        {
            ToEmails = new List<MailboxAddress>();

            ToEmails.AddRange(toEmails.Select(x => new MailboxAddress(x, x)));
            CcEmails.AddRange(ccEmails.Select(x => new MailboxAddress(x, x)));
            Subject = subject;
            Body = body;
        }

        public MailRequest(string subject, string body)
        {
            Subject = subject;
            Body = body;
        }

        public MailRequest(string body)
        {
            Body = body;
        }
    }
}
