﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TIM.MERCEDES.ENTITIES.Concrete.Models
{
    public class GetBBDataFromS2GatherRequest: RequestBase
    {
        public DateTime startDate { get; set; }
        public DateTime endDate { get; set; }
    }
}
