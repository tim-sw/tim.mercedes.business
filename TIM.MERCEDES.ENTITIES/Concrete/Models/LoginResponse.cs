﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TIM.MERCEDES.ENTITIES.Concrete.Models
{
    public class LoginResponse : ResponseBase
    {
        public bool isAuthenticationValid { get; set; } = false;
    }
}
