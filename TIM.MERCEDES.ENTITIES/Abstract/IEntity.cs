﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TIM.MERCEDES.ENTITIES.Abstract
{
    public interface IEntity
    {
        public DateTime CreatedOn { get; set; }
        public DateTime ModifiedOn { get; set; }
        public string CreatedBy { get; set; }
        public string ModifiedBy { get; set; }
    }
}
